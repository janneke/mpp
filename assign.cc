/****************************************************************************
  PROJECT: MusiXTeX PreProcessor
  FILE   : assign.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
-----------------------------------------------------------------------------
  ABSTRACT: determine associativity of assignment operator
            a return value of 1 means (faulty) left to right associativity
--*/

#ifdef TEST

#include <iostream.h>

int
main()
    {
    int i = 1;
    int j = 1;
    i = j = 0;
    cout << "assign: " << i << endl;
    return i;
    }

#endif
