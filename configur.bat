@echo off
: PROJECT: MusixTeX PreProcessor
: FILE   : configur.bat 
: AUTHOR : JCNieuwenhuizen
:
: copyright (c) FlowerSoft 1995, 1996, 1997
: ---------------------------------------------------------------------------
: ABSTRACT: script to configure mpp and to determine specifics of C++ release
: --

set platform=%1

if not "%1" == "" goto :platformset
set platform=msvc

:platformset
echo PLATFORM = %platform%> Makefile
type config\Makefile.0 >> Makefile
copy config\%platform%\config.hh src > NUL
call config\%platform%\setenv.bat
set systemname=src\systemna.hh
echo "your dos version here" > %systemname%

echo configured for %platform%
