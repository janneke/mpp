#!/usr/local/bin/perl
#
# ^^^ where ever your perl may be
#
# read lyrics file (format is words/sylables seperated by space)
# and add it to piped in data
# expect already processed musixtex from mpp
#

$lfile = shift || die "usage: $0 <lyric file>\n";

open(LYRICS, "./$lfile") || die "cannot open $lfile\n";

while(<LYRICS>) {
    chop;
    push(@lyrics, (split(/ /, $_)));
}

#an incomplete list of expected rests (but enough for this example)

@restlist = (   "qs",
                "cs",
                "hs"
            );
while(<>) {
    $inputline = $_;
    if (/^\\N/) {
        @line = split(/\\/);
        $begin = shift(@line);
        if ($begin eq '') {
            $begin = shift(@line);
        }
        print "\\$begin";
        $" = '\\';
        if ($line[0] =~ /^tslur/ || $line[1] =~ /^tslur/) {
           print "\\@line";
        }
        elsif (grep($inputline =~ /$_/, @restlist)) {
            print "\\@line";
        }
        else {
            $nextword = shift(@lyrics);
            print "\\medtype\\hsong{$nextword}\\@line";
        }
    }
    else {
        print "$_";
    }
}

