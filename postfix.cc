/****************************************************************************
  PROJECT: MusiXTeX PreProcessor
  FILE   : postfix.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
-----------------------------------------------------------------------------
  ABSTRACT: determine syntaxis of postfix operator
  	    a return value of 1 means (obsolete) release 2.0 style syntaxis
--*/

#ifdef TEST

#include <iostream.h>

class Integer
    {
    int i;

 public:
    operator int&()
        {
        return i;
        }

#if 1

    int operator --()          // prefix
        {
        return i;
        }

#else

    int operator --( int )     // postfix
        {
        return --i;
        }

#endif

    int operator ++()
        {
        return i;
        }

    Integer( int _i )
        {
        i = _i;
        }
    };

int
main()
    {
    Integer i = 1;
#if 1
    int k = ( i )--;
    int j = i;                 // release 2.0: j = i.operator--() = 1
                               // release 2.1: j = ( i.operator int() )-- = 0
#else
    int j = --i;               // release 2.0: j = i.operator--() = 1
#endif
    cout << "postfix: " << j << endl;
    return j;
    }

#endif
