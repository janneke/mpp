/****************************************************************************
  PROJECT: FlowerSoft C++ library
  FILE   : array.cc
--*/
#include "globals.hh"
#include "array.hh"

/****************************************************************************
  class Array
--*/

Array::Array( int t, int b, int d )
{
    _bottom = cursor = b;
    _top = t;
    delta = d;
    array = new Object *[ size() ];
    for( int i = 0; i < size(); i++ )
        array[ i ] = ZERO;
}

int
Array::operator ==( const Object& test )
{

    if ( _bottom != ( (Array&)test )._bottom ||
        _top != ( (Array&)test )._top )
        return 0;

    for ( int i = 0; i < size(); i++ )
        {
        if ( array[ i ] != ZERO )
            {
            if ( ( (Array&)test ).array[ i ] != ZERO )
                {
                if ( *array[ i ] != *( ( (Array &)test ).array[ i ] ) )
                    return 0;
                }
            else
                return 0;
            }
        else
            if ( ( (Array&)test ).array[ i ] != ZERO )
                return 0;
        }
    return 1;
}

void
Array::clear()
{
    for( int i = 0; i < size(); i++ )
        if( array[ i ] != ZERO )
            delete array[ i ];
}
                               // similar to class List !!
void
Array::each( ITERATE action, void* parameters )
{
    ArrayIterator items( *this );
    while( items )
        items++.each( action, parameters );
}

                               // similar to class List !!
void
Array::each( ITERATE_CONST action, void* parameters ) const
{
    ArrayConstIterator items( *this );
    while( items )
        items++.each( action, parameters );
}
                               // similar to class List !!
Object& Array::first( ITERATE_TEST test, void* parameters )
{
    ArrayIterator items( *this );
    while( items )
        {
        Object& object = items++.first( test, parameters );
        if ( object != NOOBJECT )
            return object;
        }
    return NOOBJECT;
}

Object& Array::get()
{
    while( ( cursor > _bottom ) && ( array[ cursor ] == ZERO ) )
        cursor--;

    if ( ( cursor > _bottom ) && ( array[ cursor - 1 ] != ZERO ) )
        {
        Object& object = *array[ cursor - 1 ];
        array[ cursor - 1 ] = ZERO;
        _count--;
        return object;
        }
    else
        return NOOBJECT;
}

Object& Array::get( int index )
{
    if ( array[ index - _bottom ] != ZERO )
        {
        Object& object = *array[ index - _bottom ];
        array[ index - _bottom ] = ZERO;
        _count--;
        return object;
        }
    else
        return NOOBJECT;
}

int
Array::index( ITERATE_TEST test, void* parameters )
{
    for ( int i = 0; i < size(); i++ )
        {
        Object& object = *array[ i ];
        if ( object != NOOBJECT )
            if( object.first( test, parameters ) != NOOBJECT )
                return i + _bottom;
        }
    return _bottom - 1;
}

void
Array::printOn( ostream& ) const
{
}

int
Array::put( Object& object )
{
    assert(array);
    while( ( cursor <= _top ) && ( array[ cursor ] != ZERO ) )
        cursor++;

    if( cursor > _top )
        if ( reallocate( cursor - _bottom + 1 ) )
            return 1;

    array[ cursor++ ] = &object;
    _count++;

    return 0;
}

int
Array::put( Object& object, int index )
{
    if( index > _top )
        if ( reallocate( index - _bottom + 1 ) )
            return 1;

    if ( array[ index ] != ZERO )
        {
        delete array[ index ];
        _count--;
        }

    array[ index ] = &object;
    _count++;

    return 0;
}

int
Array::reallocate( int newSize )
{
    if ( delta == 0 )
        {
                               // might change to warning
        error( "attempt to resize fixed array", __FILE__, __LINE__ );
        return 1;
        }

//    assert ( newSize > size() );

    int adjustedSize = newSize + ( delta - ( newSize % delta ) );

    Object **newArray = new Object *[ adjustedSize ];
    if ( !newArray )
        error( "out of memory", __FILE__, __LINE__ );

    int i;
    for ( i = 0; i < size(); i++ )
        newArray[ i ] = array[ i ];

    for ( ; i < adjustedSize; i++ )
        newArray[ i ] = ZERO;

    DELETE_ARRAY( size() ) array;

    array = newArray;
    _top = adjustedSize + _bottom - 1;

   return 0;
}

//-- class Array //

