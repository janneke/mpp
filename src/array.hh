// -*-C++-*-
/****************************************************************************
  PROJECT: FlowerSoft C++ library
  FILE   : array.h
--*/

#ifndef __ARRAY_H
#define __ARRAY_H


#include "object.hh"


class Array : public Object
{
 friend  class ArrayIterator;

    Array(Array&) {  } // disable

 protected:
    Object& item( int i ) const;
    int reallocate( int newSize );
    int delta;
    int _bottom;
    int _top;
    int _count;
    int cursor;
    Object **array;


 public:
    int operator ==( const Object& test );
    Object& operator []( int index ) const;

    int bottom() const;
    void clear();
    int count() const;
    void each( ITERATE action, void* parameters = 0 );
    void each( ITERATE_CONST action, void* parameters = 0 ) const;
    int empty();
    Object& first( ITERATE_TEST test, void* parameters = 0 );
    Object& get();
    Object& get( int index );
    virtual int index( ITERATE_TEST test, void* parameters = 0 );
    int top() const;
    int size() const;
    virtual void printOn( ostream& ) const;
    int put( Object& object );
    int put( Object& object, int index );

    Array( int t, int b = 0, int d = 0 );
    virtual ~Array();
};

inline int Array::size() const
{
    return ( _top - _bottom + 1 );
}

inline Array::~Array()
{
    clear();
    DELETE_ARRAY( size() ) array;
}

inline int Array::bottom() const
{
    return _bottom;
}

inline int Array::count() const
{
    return _count;
}

inline int Array::empty()
{
    return ( _count == 0 );
}

inline Object&  Array::item( int i ) const
{
    return *array[ i - _bottom ];
}

inline int Array::top() const
{
    return _top;
}

inline Object& Array::operator []( int index ) const
{
    return item( index );
}
//-- class Array //


/****************************************************************************
  class ArrayIterator
--*/

class ArrayIterator : public Object
{
    int cursor;
    Array& array;

 public:
    virtual operator int();
                               // more elegant?  for List too ?
//    operator Object&();
    Object& operator ()();
    Object& operator ++( POSTFIX_INT );
    void reset();

    ArrayIterator( Array& a );
    virtual ~ArrayIterator();
};

inline ArrayIterator::ArrayIterator( Array& a ) :
    cursor( a._bottom ),
   array( a )
{
}

inline ArrayIterator::~ArrayIterator()
{
}

inline ArrayIterator::operator int()
{
    return cursor <= array._top;
}

#if 0                          // more elegant?
inline ArrayIterator::operator Object&()
{
    return ( ( cursor <= array._top ) ?
        ( (Object&)( array.item( cursor ) ) ) : NOOBJECT );
}

#else

inline Object& ArrayIterator::operator ()()
{
    return ( ( cursor <= array._top ) ?
        ( (Object&)( array.item( cursor ) ) ) : NOOBJECT );
}
#endif

inline void ArrayIterator::reset()
{
    cursor = array._bottom;
}

inline Object& ArrayIterator::operator ++( POSTFIX_INT )
{
    if ( cursor <= array._top )
        {
        cursor++;
        return ( (Object&)( array.item( cursor - 1 ) ) );
        }
    else
        return NOOBJECT;
}
//-- class ArrayIterator //

/****************************************************************************
  class ArrayConstIterator
--*/

class ArrayConstIterator : private ArrayIterator
{
 public:
    virtual operator int();
    const Object& operator ()();
    const Object& operator ++( POSTFIX_INT );

    ArrayIterator::reset;

    ArrayConstIterator( const Array& a );
    virtual ~ArrayConstIterator();
};

inline ArrayConstIterator::ArrayConstIterator( const Array& array ) :
    ArrayIterator( (Array&) array )
{
}

inline ArrayConstIterator::~ArrayConstIterator()
{
}

inline ArrayConstIterator::operator int()
{
    return ( (ArrayIterator&) *this );
}

#if 0                          // more elegant?
inline ArrayConstIterator::operator const Object&()
{
    return ( (ArrayIterator&) *this );
}

#else

inline const Object& ArrayConstIterator::operator ()()
{
    return ( (ArrayIterator&) *this )();
}
#endif

inline const Object& ArrayConstIterator::operator ++( POSTFIX_INT )
{
    return ( (ArrayIterator&) *this )++;
}
//-- class ArrayConstIterator //

#endif // __ARRAY_H //
