/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : bar.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#include <assert.h>
#include "interval.hh"
#include "duration.hh"
#include "maclist.hh"
#include "strlist.hh"
#include "clef.hh"
#include "note.hh"
#include "beamnote.hh"          // last
#include "bar.hh"
#include "init.hh"
#include "staff.hh"
#include "mpp.hh"

/****************************************************************************
  class Bar
--*/

Bar::Bar() :
    barToken( 0 ),
    macroList( *new MacroList() )
{
}

Bar::Bar( Bar& bar ) :
    barToken( 0 ),
    macroList( *new MacroList( bar.macroList ) ), // ?? why dynamic?
    NoteList( bar )
{
}

Bar::Bar( Staff& staff ) :
    barToken( 0 ),
    macroList( *new MacroList() ), 	// Why dynamic??
    NoteList()
{
    staff.bar = this;          // is this *really* necessary ?

    if ( getFrom( staff ) )			  // access infile
        staff.warning( "bar: unexpected end of file" );
    else
        {
        barToken = new BarToken( staff );
        barToken->execute( *(StringList*)ZERO, staff );
        ;// monitor << "after BarToken->execute" << endl;
        }

    ;// monitor << "leaving Bar::Bar" << endl;
}

Bar::~Bar()
{
    if ( barToken )
        delete barToken;
    delete &macroList;
}

void
Bar::calculate()
{
    BeamNote::last = &NOBEAMNOTE;
    each( Note::invokeCalculate );
}


int Bar::duration2NoteSkip( BarIterator& notes, int duration ) const
//int
//Bar::duration2NoteSkip( BarIterator& notes, int duration )
{

    int noteSkip = 0;
    while ( notes && (duration>0) ) /* Safer condition, MATSB */
        {
        Note* note = &(notes++);
        duration -= note->duration();
//        noteSkip += note->duration->noteSkip();
        Duration d( note->duration() );
        noteSkip += d.noteSkip();
        }

    return noteSkip;
}

//int Bar::duration2NoteSkip( const Interval& duration ) const
int
Bar::duration2NoteSkip( const Interval& duration )
{
    BarIterator notes( *this );

    int d = duration.begin;
    duration2NoteSkip( notes, d );
    d = duration.length;

    return duration2NoteSkip( notes, d );
}

// could be in ctor.
int
Bar::getFrom( Staff& staff )
{
    istream& is = *staff.is;

    do
        {
	MacroList addthis(staff);
        macroList.putList(addthis);
        
        WhiteSpace ws( staff );

        if ( ws.length < 0 )   // end of file
            return 1;

        char c = is.peek();
                               // if end of bar
        if ( ( c == ':' ) || ( c == '|' ) )
            return 0;
        else if ( c == (char)EOF )
            return 1;
        else
            {
            Note& note = Note::getNote( staff, macroList );
            put( note );
            }

        c = is.peek();
	
	
        if ( c == (char)EOF ) // end of file // cast??
            return 1; // en hierondre?
        } 
        while ( !is.eof() );

    return 0;
}

//int Bar::noteCount2Duration( BarIterator& notes, int noteCount ) const
int
Bar::noteCount2Duration( BarIterator& notes, int noteCount )
{
    int duration = 0;

    while ( notes && noteCount )
        {
        Note* note = &(notes++);
	assert ( (void*)note != (void*)ZERONOTE );
        duration += note->duration();
        noteCount--;
        }

    return duration;
}

//Interval& Bar::noteCount2Duration( const Interval& note ) const
Interval& Bar::noteCount2Duration( const Interval& note )
{
    BarIterator notes( *this );
    Interval& duration = *new Interval;

    int noteCount = note.begin - 1;

    assert ( noteCount>= 0 );


    duration.begin = noteCount2Duration( notes, noteCount );
    noteCount = note.length;
    duration.length = noteCount2Duration( notes, noteCount );

    return duration;
}

						  // supfl.
void
Bar::printOn( ostream& os ) const
{
    List::printOn( os );
    os << macroList;
    if ( barToken )
        os << *barToken;
#if 0
    if ( Staff::changeContext )
        {
        os << "\\zchangecontext";
        Staff::changeContext = 0;
        }
#endif
}
//-- class Bar //





