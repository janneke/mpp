// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : bar.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __BAR_H
#define __BAR_H

#include "macro.hh"



#include "notelist.hh"


class istream;
class ostream;
class BarToken;
class Interval;
class Staff;

#define BarIterator NoteListIterator
class BarIterator;

/****************************************************************************
  class Bar
--*/
/*
   should be expanded
 */
class Bar : public NoteList
{
friend class Score;

protected:
    BarToken* barToken;
    MacroList& macroList;

    int duration2NoteSkip( BarIterator& notes, int duration ) const ;
    virtual int getFrom( Staff& staff );
    int noteCount2Duration( BarIterator& notes, int noteCount );

public:
    virtual void calculate();
    int duration2NoteSkip( const Interval& duration );
    virtual void printOn( ostream& os ) const;
    Interval& noteCount2Duration( const Interval& note );

    Bar();
    Bar( Bar& bar );
    Bar( Staff& staff );
    virtual ~Bar();
};
//-- class Bar //

#endif // __BAR_H //
