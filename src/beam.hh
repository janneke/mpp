// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : beam.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __BEAM_H
#define __BEAM_H

#include "globals.hh"
#include "macro.hh"
#include "intlist.hh"

/****************************************************************************
  class InitiateBeam
--*/

class InitiateBeam : public Initiator
{
 friend class InitMultiBeam;
 friend class TermMultiBeam;
 friend class TerminateBeam;
 friend class Feature;
 friend class SimpleNote;      // triplet, access noPrint

 public:
    enum Flag
    {
        NO, RIGHT, LEFT
    };

 private:
    Staff& staff;
    InitiateBeam& mother;

    static const int MAXCORRECT;
    static const int NORMALSLOPE;
    static const int NORMALNOTESKIP;
    static const int NORMALSTEM;

    int noPrint;
    IntegerList& pitchList;
    int slope;
    int slopeCalculation;

 public:
    static int minimumStem;
    static int noteSkipFactor;
    static int slopeFactor;

    int multiplicity;
    int noteCount;
    int noteStart;
    int nPlet;
    int number;
    Orientation orientation;
    int pitch;

    static InitiateBeam& getInitiateBeam( Staff& staff );
    void addPitch( int p );
    void calculate();
    void calculateOrientation();
    void calculatePitch();
    void calculateSlope();
    int effectivePitch( const int n ) const;
    int musixSlope() const;
    virtual void printOn( ostream& os ) const;
    void setSlope( int s );

    InitiateBeam( Staff& s );
    InitiateBeam( InitiateBeam& mother );
    virtual ~InitiateBeam();
};

#define ZEROBEAM ((InitiateBeam*)ZERO)
#define NOBEAM (*ZEROBEAM)
//-- class InitiateBeam //


/****************************************************************************
  class TerminateBeam
--*/

class TerminateBeam : public Terminator {
 friend class Note;            // for unfriendly changeing of number
 friend class SimpleNote;      // triplet, access noPrint

    Staff& staff;
    InitiateBeam& mother;
    int noPrint;
    int number;
    Orientation orientation;

 public:
    static TerminateBeam& getTerminateBeam( Staff& staff );
    virtual void printOn( ostream& os ) const;

    TerminateBeam( Staff& s );
    TerminateBeam( InitiateBeam& m );
    virtual ~TerminateBeam();
};

#define ZEROTERMINATEBEAM ((TerminateBeam*)ZERO)
#define NOTERMINATEBEAM (*ZEROTERMINATEBEAM)
//-- class TerminateBeam //

/****************************************************************************
  class InitMultiBeam
--*/

class InitMultiBeam : public Initiator
{
    InitiateBeam& mother;
    int multiplicity;
    NESTED_IN( InitiateBeam )Flag singleFlag;

 public:
    virtual void printOn( ostream& os ) const;

    InitMultiBeam( InitiateBeam& m, int mult, NESTED_IN( InitiateBeam )Flag single = InitiateBeam::NO );
    ~InitMultiBeam();
};
//-- class InitMultiBeam //

/****************************************************************************
  class TermMultiBeam
--*/

class TermMultiBeam : public Terminator {
    InitiateBeam& mother;
    int multiplicity;
    NESTED_IN( InitiateBeam )Flag singleFlag;

public:
    virtual void printOn( ostream& os ) const;

    TermMultiBeam( InitiateBeam& m, int mult, NESTED_IN( InitiateBeam)Flag single = InitiateBeam::NO );
    ~TermMultiBeam();
};
//-- class TermMultiBeam //

#endif // __BEAM_H //


