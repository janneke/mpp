/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : beamlist.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include "beamlist.hh"

/****************************************************************************
  class BeamList
--*/

implement( ReadableList, InitiateBeam );

//-- class BeamList //

/****************************************************************************
  class TerminateBeamList
--*/

implement( ReadableList, TerminateBeam );

//-- class TerminateBeamList //
