#ifndef __BEAMLIST_H
#define __BEAMLIST_H

#include "grlist.hh"



#include "beam.hh"


declare( List, InitiateBeam );
declare( ReadableList, InitiateBeam );
declare( List, TerminateBeam );
declare( ReadableList, TerminateBeam );

#ifndef BeamList
#define BeamList GENERIC( InitiateBeam, READABLE( List ) )
#define BeamListIterator GENERIC( InitiateBeam, READABLE( ListIterator ) )

#define TerminateBeamList GENERIC( TerminateBeam, READABLE( List ) )
#define TerminateBeamListIterator GENERIC( TerminateBeam, READABLE( ListIterator ) )
#endif

#endif //__BEAMLIST_H //
