// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : beamnote.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __BEAMNOTE_H
#define __BEAMNOTE_H


#include "simpnote.hh"


/****************************************************************************
  class BeamNote
--*/

class BeamNote : public SimpleNote
{
 friend class Bar;             // initialize last
 friend class InitiateSlur;    // beamNoteCount

    int beamNoteCount;
    int continuedBeamMultiplicity;
    static BeamNote* last;

    void calculateBeam();
    void decreaseMultiplicity();
    void increaseMultiplicity();

 protected:
    virtual void calculateFeaturePitch();
    virtual void calculateStem();
    virtual void printDurationOn( ostream& os ) const;
    virtual void printNameOn( ostream& os ) const;
    virtual void printStemOn( ostream& os ) const;

 public:
    virtual void calculate();
    virtual void printOn( ostream& os ) const;

    BeamNote( SimpleNote& note );
    virtual ~BeamNote();
};

#define ZEROBEAMNOTE ((BeamNote*)ZERO)
#define NOBEAMNOTE (*ZEROBEAMNOTE)

//-- BeamNote //


/****************************************************************************
  class Rest
--*/

class Rest : public SimpleNote
{
friend class SimpleNote;
    static int _pitch;

protected:
    virtual void printNameOn( ostream& os ) const;
    virtual void printStemOn( ostream& os ) const;

public:
    virtual void calculate();
    virtual void calculateFeaturePitch();
    virtual int pitch() const;
    virtual void printOn( ostream& os ) const;

    Rest( SimpleNote& note );
    virtual ~Rest();
};
//-- Rest //


/****************************************************************************
  class GhostNote
--*/

class GhostNote : public SimpleNote
{
friend class SimpleNote;
    static int _pitch;

public:
    virtual void calculate();
    virtual void calculateFeaturePitch();
    virtual int pitch() const;
    virtual void printOn( ostream& os ) const;

    GhostNote( SimpleNote& note );
    virtual ~GhostNote();
};
//-- GhostNote //

#endif // __BEAMNOTE_H //
