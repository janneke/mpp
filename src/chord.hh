// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : chord.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __CHORD_H
#define __CHORD_H


#include "noteslis.hh"


/****************************************************************************
  class Chord
--*/

class Chord : public virtual Note, public virtual NoteSortedList
{
 friend class Note;

    void calculateChordStem( SimpleNote& high, SimpleNote& low );
    void calculateSignSpacing();
    void calculateShift( SimpleNote& high, SimpleNote& low );
    void calculateStem();

 public:
    void* operator new( size_t size )
                              { return Note::operator new( size ); }

    virtual void calculate();
    virtual int duration();
    virtual int pitch() const;
    static Chord& getChord( Staff& staff );
    virtual void printOn( ostream& os ) const;
    virtual int spacing( int additionalSpacing );

    Chord( Staff& s );
    virtual ~Chord();
};

#define ZEROCHORD ((Chord*)ZERO)
#define NOCHORD   (*ZEROCHORD)

#define ChordIterator NoteSortedListIterator
//-- Chord //

#endif // __CHORD_H //
