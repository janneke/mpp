#ifndef __CHORDLIST_H
#define __CHORDLIST_H

#include "glist.hh"



#include "chord.hh"


declare( List, Chord );
#define ChordList GENERIC( Chord, List )
#define ChordListIterator GENERIC( Chord, ListIterator )

#endif //__CHORDLIST_H //
