#ifndef __CITERATOR_H
#define __CITERATOR_H

#include "list.hh"


/****************************************************************************
  class ListConstIterator
--*/

class ListConstIterator : private ListIterator
{
public:
    void* operator new( size_t size );
    virtual operator int() const;
    const Object& operator ()();
    const Object& operator ++( POSTFIX_INT );

    ListIterator::printOn;
    ListIterator::reset;

    ListConstIterator( const List& list );
    virtual ~ListConstIterator();
};

inline void* ListConstIterator::operator new( size_t size )
{
   return ListIterator::operator new( size );
}

inline ListConstIterator::ListConstIterator( const List& list ) :
                               // we promise not to change list
    ListIterator( (List&)list )
{
}

inline ListConstIterator::~ListConstIterator()
{
}

inline ListConstIterator::operator int() const
{
    return ( (ListIterator&) *this );
}

inline const Object& ListConstIterator::operator ()()
{
    return ( (ListIterator&) *this )();
}

inline const Object& ListConstIterator::operator ++( POSTFIX_INT )
{
    return ( (ListIterator&) *this )++;
}

//-- class ListConstIterator //


#endif //__CONSTITERATOR //
