/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : clef.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include <ctype.h>
#include "init.hh"
#include "note.hh"
#include "staff.hh"
#include "key.hh"
#include "clef.hh"
#include "strlist.hh"
#include "mpp.hh"


/****************************************************************************
  class BarToken
--*/

BarToken::BarToken( String s, String sub ) :
    Macro( s, sub )
{
}

BarToken::BarToken( Staff& staff ) :
    Macro( "" )
{
    ;// monitor << "BarToken::BarToken" << endl;
    Macro::getFrom( staff );
    ;// monitor << "leaving BarToken::BarToken" << endl;
}

BarToken::~BarToken()
{
}

void
BarToken::execute( StringList&, Staff& staff )
{
    ;// monitor << "BarToken::execute" << endl;

    if ( staff.key && !staff.changeContext )
        staff.key->execute( *(StringList*)ZERO, staff );

    ;// monitor << "leaving BarToken::execute" << endl;
}

void
BarToken::printOn( ostream& os ) const
{
    ;// monitor << "BarToken::printOn" << endl;

    if ( Staff::newLine )
        {
        Staff::newLine = 0;
        return;
        }

    if ( Staff::changeContext )
        {
        if ( String( name ) == "|" )
//            os << "\\changecontext\\addspace{-\\afterruleskip}%";
            os << "\\changecontext%";
        else if ( String( name ) == "||" )
//            os << "\\Changecontext\\addspace{-\\afterruleskip}%";
            os << "\\Changecontext%";
        else
            {
            os << substitute;
            os << "\\zchangecontext%";
            }
        Staff::changeContext = 0;
    }
    else
        os << substitute;
}

int
BarToken::validCharacter( const char c ) const
{
    return ( Token::validCharacter( c ) &&
        ( ( c == ':' ) || ( c == '|' ) || ( c == '.' ) ) );
}
//-- class BarToken //

/****************************************************************************
  class Clef
--*/

Clef::Clef( String s, int p, String sub ) :
    midPitch( p ),
    Macro( s, sub )
{
//    if ( Staff ::c urrent )
//        Staff ::c urrent->midPitch = midPitch;
}

Clef::~Clef()
{
}

void
Clef::execute( StringList&, Staff& staff )
{
    staff.midPitch = midPitch;
    substitute = "\\setclef";
    substitute += String( staff.count - staff.number );
    substitute += "\\";
    substitute += name;
    if ( staff.barCount )
	substitute += "\\changeclefs";//\\qsk";
    else
    substitute += "%\n";
}
//-- class Clef //
