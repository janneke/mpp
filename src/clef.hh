// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : clef.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __CLEF_H
#define __CLEF_H


#include "macro.hh"


/****************************************************************************
  class BarToken
--*/

enum BarType {
  BAR_SINGLE, BAR_DOUBLE, BAR_RIGHT, BAR_LEFT, BAR_RIGHTLEFT,
  BAR_DOUBLE_RIGHTLEFT
};
class BarToken : public Macro {

protected:
    virtual int validCharacter( const char c ) const;

public:
//    virtual void execute( StringList& parameterList = *(StringList*)ZERO, Staff& staff = *(Staff*)ZERO );
    virtual void execute( StringList& parameterList, Staff& staff );
    virtual void printOn( ostream& os ) const;

    BarToken( String s, String sub = "" );
    BarToken( Staff& staff );
    virtual ~BarToken();
};

//-- BarToken //

/****************************************************************************
  class Clef
--*/

class Clef : public Macro {

    int midPitch;
public:
//    virtual void execute( StringList& parameterList = *(StringList*)ZERO, Staff& staff = *(Staff*)ZERO );
    virtual void execute( StringList& parameterList, Staff& staff );

    Clef( String s, int p = 0, String sub = "" );
    virtual ~Clef();
};
//-- class Clef //

#endif // __CLEF_H //
