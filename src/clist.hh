#ifndef __CLIST_H
#define __CLIST_H

#include "list.hh"


/****************************************************************************
  class ConstList
--*/

class ConstList : private List
{
friend class ConstListIterator;

    const Link& _top() const;

public:
    void *operator new( size_t size );

    const Object& bottom() const;
    int count() const;
//    void each( ITERR action, void *parameters );
    int empty() const;
//    Object& first( CITERR test, void *parameters );
    const Object& next() const;
    virtual void printOn( ostream& os ) const;
    const Object& top() const;

    ConstList( const List& list );
    virtual ~ConstList();
};

ConstList::ConstList( const List& list ) :
    List( ( List&)list )
{
}

inline ConstList::~ConstList()
{
}

/* inline */ void* ConstList::operator new( size_t size )
{
    return Object::operator new( size );
}

inline const Link& ConstList::_top() const
{
    return List::_top();
}

inline const Object& ConstList::bottom() const
{
    return List::bottom();
}

inline int ConstList::count() const
{
    return List::count();
}

inline int ConstList::empty() const
{
    return List::empty();
}

inline const Object& ConstList::next() const
{
    return List::next();
}

inline void ConstList::printOn( ostream& os ) const
{
    List::printOn( os );
}

inline const Object& ConstList::top() const
{
    return List::top();
}
//-- class ConstList //

/****************************************************************************
  class ConstListIterator
--*/

class ConstListIterator : public Object
{
    ConstList list;
    const Link* cursor;

public:
    virtual operator int();
    const Object& operator ()();
    const Object& operator ++( POSTFIX_INT );

    virtual void printOn( ostream& os ) const;
    virtual void reset();

    ConstListIterator( const List& l );
    virtual ~ConstListIterator();
};

inline ConstListIterator::ConstListIterator( const List& l ) :
    list( l )
{
    cursor = &list._top();
}

inline ConstListIterator::~ConstListIterator()
{
}

inline ConstListIterator::operator int()
{
    return cursor != ZEROLINK;
}

inline const Object& ConstListIterator::operator ()()
{
    return ( ( cursor != ZEROLINK ) ? ( cursor->object ) : NOOBJECT );
}

inline const Object& ConstListIterator::operator ++( POSTFIX_INT )
{
    Object *object = ( ( cursor != ZEROLINK ) ? &( cursor->object ) : ZERO );
    if ( list.last != ZEROLINK )
        cursor = ( cursor != list.last ) ? cursor->next : ZEROLINK;
    return *object;
}

inline void ConstListIterator::reset()
{
    cursor = &list._top();
}

inline void ConstListIterator::printOn( ostream& os ) const
{
    os << "ConstListIterator";
}

//-- class ConstListIterator //

#endif //__CLIST //
