/* Generated automatically by configure */
#ifndef __CONFIG_HH
#define __CONFIG_HH
#ifndef __unix__ 
#define __unix__ 
#endif 
#define PATHSEP '/'
#define POSTFIX_INT int
#define DELETE_ARRAY( size ) delete[]
#define NESTED_IN( Class ) Class::
#endif /* __CONFIG_HH */
