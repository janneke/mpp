// -*-C++-*-
/****************************************************************************
  PROJECT: FlowerSoft C++ library
  FILE   : cplusplu.h
--*/
#ifndef __CPLUSPLUS_H
#define __CPLUSPLUS_H

#include "config.hh"

#ifndef __CPLUSPLUS_RELEASE__
#if defined( __TURBOC__ ) && ( __TURBOC__ < 0x300 )
                               // release 2.0
#define __CPLUSPLUS_RELEASE__ 20
#else
                               // release 3.0
#define __CPLUSPLUS_RELEASE__ 30
#endif
#endif


// ?? ARM: identifiers have unlimited length!
#ifndef IDENTIFIER_MAX
#ifdef __TURBOC__
#define IDENTIFIER_MAX 32      // stupid turboc
#else
#define IDENTIFIER_MAX 200     //
#endif
#endif


// wharg
#if __CPLUSPLUS_RELEASE__ < 21
#define DELETE_ARRAY( size ) delete[ size ]
#define COMMA_RIGHT_TO_LEFT
#ifndef POSTFIX_INT
#define NESTED_IN( Class )
#define POSTFIX_INT
#endif
#else
#define DELETE_ARRAY( size ) delete[]
#ifndef POSTFIX_INT
#define NESTED_IN( Class ) Class::
#define POSTFIX_INT int
#endif
#endif

#endif // __CPLUSPLUS_H  //
