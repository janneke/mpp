/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : cresc.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include "cresc.hh"
#include "init.hh"
#include "staff.hh"
#include "mpp.hh"

/****************************************************************************
  class InitiateCrescendo
--*/

//InitiateCrescendo& getInitiateCrescendo( Staff& staff )
InitiateCrescendo& InitiateCrescendo::getCrescendo( Staff& staff )
{
    ;// monitor << "InitiateCrescendo::getCrescendo" << endl;
    WhiteSpace ws( staff );
    istream& is = *staff.is;
    char c = is.peek();
    switch ( c )
        {
        case '<'  :
            is.get( c );
            c = is.peek();
            if ( c == '!' )
                {
                is.get( c );
                return *new TerminateCrescendo();
                }
            else
                return *new InitiateCrescendo();
        case '>'  :
            is.get( c );
            c = is.peek();
            if ( c == '!' )
                {
                is.get( c );
                return *new TerminateDecrescendo();
                }
            else
                return *new InitiateDecrescendo();
        default    :
            return NOCRESCENDO;
        }
}

InitiateCrescendo::InitiateCrescendo() :
    Script( CRESCENDO )
{
    ;// monitor << "InititateCrescendo" << endl;
//  name = "\\sk\\icresc\\bsk";
    name = "\\roffset{2}\\icresc";
}

InitiateCrescendo::~InitiateCrescendo()
{
    ;// monitor << "~InitiateCrescendo" << endl;
}

void
InitiateCrescendo::calculate( SimpleNote& note )
{
    ;// monitor << "InitiateCrescendo::printOn" << endl;
    if ( orientation == UNDEFINED )
        orientation = DOWN;

    Script::calculate( note );
}

void
InitiateCrescendo::printOn( ostream& os ) const
{
    ;// monitor << "InitiateCrescendo::printOn" << endl;
//    name = "\\zchar{-9}\\tcresc";
//    os << name;
    os << "\\zchar{";
    os << min( pitch + pitchAdjust, -9 );
    os << "}{";
    os << name;
    os << "}";
}
//-- class InitiateCrescendo //

/****************************************************************************
  class TerminateCrescendo
--*/

TerminateCrescendo::TerminateCrescendo() :
    InitiateCrescendo()
{
    ;// monitor << "TerminateCrescendo" << endl;
    name = "\\tcresc";
}

TerminateCrescendo::~TerminateCrescendo()
{
    ;// monitor << "~TerminateCrescendo" << endl;
}
//-- class TerminateCrescendo //


/****************************************************************************
  class InitiateDecrescendo
--*/

InitiateDecrescendo::InitiateDecrescendo() :
    InitiateCrescendo()
{
    ;// monitor << "InititateDecrescendo" << endl;
//    name = "\\sk\\idecresc\\bsk";
    name = "\\roffset{2}\\idecresc";
}

InitiateDecrescendo::~InitiateDecrescendo()
{
    ;// monitor << "~InitiateDecrescendo" << endl;
}

//-- class InitiateDecrescendo //

/****************************************************************************
  class TerminateDecrescendo
--*/

TerminateDecrescendo::TerminateDecrescendo() :
    InitiateCrescendo()
{
    ;// monitor << "TerminateDecrescendo" << endl;
    name = "\\tdecresc";
}

TerminateDecrescendo::~TerminateDecrescendo()
{
    ;// monitor << "~TerminateDecrescendo" << endl;
}
//-- class TerminateDecrescendo //
