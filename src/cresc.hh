// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : cresc.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __CRESC_H
#define __CRESC_H

#include "script.hh"


class istream;
class ostream;

/****************************************************************************
  class InitiateCrescendo
--*/

class InitiateCrescendo : public Script
{
 protected:
    virtual void printOn( ostream& os ) const;

 public:
//    static InitiateCrescendo& getInitiateCrescendo( Staff& staff );
    static InitiateCrescendo& getCrescendo( Staff& staff );

    virtual void calculate( SimpleNote& note );

    InitiateCrescendo();
    virtual ~InitiateCrescendo();
};

#define ZEROCRESCENDO ((InitiateCrescendo*)ZERO)
#define NOCRESCENDO (*ZEROCRESCENDO)

//-- class InitiateCrescendo //

/****************************************************************************
  class TerminateCrescendo
--*/

class TerminateCrescendo : public InitiateCrescendo
{
 public:
    TerminateCrescendo();
    virtual ~TerminateCrescendo();
};
//-- class TerminateCrescendo //


/****************************************************************************
  class InitiateDecrescendo
--*/

class InitiateDecrescendo : public InitiateCrescendo
{
 public:
    InitiateDecrescendo();
    ~InitiateDecrescendo();
};
//-- class InitiateDecrescendo //

/****************************************************************************
  class TerminateDecrescendo
--*/

class TerminateDecrescendo : public InitiateCrescendo
{
 public:
    TerminateDecrescendo();
    ~TerminateDecrescendo();
};
//-- class TerminateDecrescendo //

#endif // __CRESC_H //
