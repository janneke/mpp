#include <fstream.h>
#include <ctype.h>

#include "textstr.hh"

Text_record
filed_stringlist::get_record() 
{
	String s;
	svec<String> fields;
	gobble_leading_white();
	
	while ((s = get_word()) != "")
	    {	    
	    fields.add(s);	
	    gobble_white();
	    }
	     

	if (get_line() != "")
	    assert(false);
	
	return Text_record(fields, get_name(), line());
}

/// gobble horizontal white stuff.
void 
data_file::gobble_white()
{
    char c;
    
    while ((c=data_get()) == ' ' ||c == '\t')
	if (eof()) 
	    break;

    data_unget(c);
}



/// read a word till next space, leave space also does quotes
String data_file::get_word() 
{// should handle escape seq's
    String s;

    while (1) 
	{
	char 	c  = data_get();
	
	if (isspace(c) || eof()) 
	    {
	    data_unget(c);
	    break;
	    }
	
	
	if (c == '\"')
	    {
	    rawmode= true;

	    while ((c  = data_get()) != '\"')
		if (eof())
		    error("EOF in a string");		
		else
		    s += c;
	    

	    rawmode= false;
	    }	    
	else
	    s += c;		
	}
    
    return s;	      
}

/// get a char.
char
data_file::data_get() {
    char c =  get(); 
    if (!rawmode && c == '#') // gobble comment
	{	
	while ((c = get()) != '\n' && !eof()) 
	    ;
	    return '\n';
	}    

    return c;
}
/**
   Only class member who uses text_file::get
   */

/// read line, gobble '\n'    
String data_file::get_line()     
{
    char c; 
    String s;

    while ((c  = data_get()) != '\n' && !eof())
	s += c;
    return s;	
}

/// gobble stuff before first entry on a line.    
void
data_file::gobble_leading_white() 
{
    char c;
    
    // eat blank lines.
    while (!eof()) 
	{
	c = data_get();		    
	if (!isspace(c))
	    break;
	}
    data_unget(c);
}


