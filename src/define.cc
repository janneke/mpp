/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : define.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include "init.hh"
#include "staff.hh"
#include "duration.hh"
#include "strlist.hh"
#include "define.hh"
#include "mpp.hh"

/****************************************************************************
  class Define
--*/

Define::Define( String s ) :
    Macro( s, "",  1 )
{
//    baseSubstitute = s;
}

Define::~Define()
{
}

void
Define::execute( StringList& parameterList, Staff& )
{
    substitute = "\\def\\the";

    substitute += name;
    substitute += "{";
    substitute += parameterList.top();
    substitute += "}%\n";
}


/****************************************************************************
  class Meter
--*/

Meter::Meter( String s ) :
    Macro( s, "",  1 )
{
}

Meter::~Meter()
{
}

void
Meter::execute( StringList& parameterList, Staff& staf)
{
    int duration = 0;
    String     s = parameterList.top();
    int inum = -1, iden =-1;

    String argument;

    if ( s == "C" )
        {
        argument = "\\meterC";
	inum = 4;
	iden = 4;
        }
    else if ( s == "C2" )
        {
        inum=2;
	iden=2;
	argument = "\\allabreve";
	}
    else
        {
        String numerator = String( s.left( s.pos( '/' ) - 1 ) );
        String denominator = String( s.right( s.len() - s.pos( '/' ) ) );
        argument = "\\meterfrac{";
        argument += numerator;
        argument += "}{";
        argument += denominator;
        argument += "}";

	inum = numerator.value();
	iden = denominator.value();

        }
    substitute = "\\generalmeter{";
    substitute += argument;
    substitute +=  "}%\n";

    if (!is_note_len(iden))
	    {
	    staf.error(String(iden)+ " is not a note length");
	    }


    Duration d( iden );
    duration = inum * d.duration(); // time intval == #notes * time(one note)

    if ( duration )
        {
        if ( Staff::barCount && Staff::barDuration )
            {
            Staff::newBarDuration = duration;
            Staff::changeContext = 1;
            }
        else
            Staff::barDuration = duration;
        }
}
//-- class Meter //

/****************************************************************************
  class Metron
--*/

Metron::Metron( String s ) :
    Macro( s, "",  1 )
{
}

Metron::~Metron()
{
}

void
Metron::execute( StringList& parameterList, Staff& )
{
    substitute = "\\def\\the";
    substitute += name;
    substitute += "{";

    String s( parameterList.top() );

    String durstr = s.left(s.pos('='));

    
    Duration duration( char2istream( s ) );


    substitute += "\\";
    substitute += duration.alpha();
    substitute += "u";
    substitute += String( 'p', duration.dots );
    substitute += "{";

    substitute += String( s.right( s.len() - s.pos( '=' ) ) );
    substitute += "}";
    substitute += "}%\n";
}
//-- class Metron //

/****************************************************************************
  class NewLine
--*/

NewLine::NewLine( String s, String sub ) :
    Macro( s, sub )
{
}

NewLine::~NewLine()
{
}

void
NewLine::execute( StringList& parameterList, Staff& staff )
{
    if ( &parameterList );

    staff.newLine = 1;
                               // this is not ok, string should be saved!
//    if ( staff.number + 1 != staff.count )
    if ( staff.number )
        substitute = "";
    else
        substitute = "\\alaligne";
}
//-- class NewLine //

/****************************************************************************
  class Volta
--*/

Volta::Volta( String s, String base ) :
    Macro( s, base, 1 )
{
}

Volta::~Volta()
{
}

void
Volta::execute( StringList& parameterList, Staff& staff )
{
    String s;

    if ( parameterList.count() )
        s = parameterList.top();

    int volta = s.value();

    if ( staff.number )
        substitute = "";
    else
        {
        if ( volta )
            {
            substitute = "\\" + baseSubstitute + "{" + s + "}";
            }
        if ( volta != 1 )
            substitute += "\\endvolta";
        }
}
//-- class Volta //









