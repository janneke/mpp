// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : define.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#ifndef __DEFINE_H
#define __DEFINE_H

#include "macro.hh"


/****************************************************************************
  class Define
--*/

class Define : public Macro {

public:
    virtual void execute( StringList& parameterList, Staff& staff );

    Define( String s );
    virtual ~Define();
};
//-- class Define //

/****************************************************************************
  class Meter
--*/

class Meter : public Macro {

public:
    virtual void execute( StringList& parameterList, Staff& staff );

    Meter( String s );
    virtual ~Meter();
};
//-- class Meter //

/****************************************************************************
  class Metron
--*/

class Metron : public Macro {

public:
    virtual void execute( StringList& parameterList, Staff& staff );

    Metron( String s );
    virtual ~Metron();
};
//-- class Metron //

/****************************************************************************
  class NewLine
--*/

class NewLine : public Macro {

public:
    virtual void execute( StringList& parameterList, Staff& staff );

    NewLine( String s, String sub );
    ~NewLine();
};
//-- class NewLine //

/****************************************************************************
  class Volta
--*/

class Volta : public Macro {
//    String baseSubstitute;

public:
    virtual void execute( StringList& parameterList, Staff& staff );

    Volta( String s, String sub );
    virtual ~Volta();
};
//-- class Volta //

#endif // __DEFINE_H //
