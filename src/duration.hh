// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : duration.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#include "globals.hh"
#ifndef __DURATION_H
#define __DURATION_H

#include "object.hh"

extern const int DURATION_WHOLE;

#if defined( __TURBOC__ ) && ( __TURBOC__ >= 0x300 )
#if !defined ( __NPLET_MAX )
#define __NPLET_MAX
extern const int NPLET_MAX;
#endif
#else // ! turboc 3.0
extern const int NPLET_MAX;
#endif

extern int onePlet;
class istream;
class ostream;
class InitiateBeam;
class Staff;

enum Note_length		/* why the _len suffix? */
    {
    whole_len = 1, half_len = 2, crotchet_len = 4,
    eighth_len = 8,sixteenth_len=16,thirtyalot_len = 32, sixtyevenmore_len =64,
    };

bool is_note_len(int n);

/****************************************************************************
  class Duration

   duration of a (note contains a duration) note
   fundamental unit -> see duration.cc
 */

// should split into a spacing obj./ musical obj.
class Duration : public Object
{
 public:                        // note, meter
    int dots;
    int multiplicity;

 private:
    InitiateBeam& beam;         // ??
    int xDuration; // ??

    int _nPlet;

    int _getFrom( istream& is );
    virtual int getFrom( Staff& staff );
//    int getNumber( istream& is );

    Note_length get_note_length(istream & is);


 public:
    String alpha() const; // ?
	int duration() const;  // naming!@#^%$#

	    // member?
    static int getNPletFrom( Staff& staff );
    int noteSkip() const;
    int nPlet() const;
    int ord() const;
    String spacing() const;
    virtual void printOn( ostream& os ) const;

    Duration( const Duration& duration );

//    Duration( const int x, const int d = 0, int& n = onePlet );
    // n = plet, d = #dots, x = notevalue (power of 2)
    Duration( const int x, const int d = 0, int n = 1 );
    Duration( istream& is );
    Duration( Staff& staff );
    virtual ~Duration(); // virt?
};
//-- class Duration  //

double duration_to_crotchets(int);

#endif // __DURATION_H  //
