/****************************************************************************
  PROJECT: FlowerSoft C++ library
  FILE   : error.cc
--*/

#include <stdlib.h>            // exit
#include <iostream.h>
#include "globals.hh"

void
error( String s, String file, int line )
{
        cerr << endl;
        if ( file != "" && file != "0" )
            cerr << file << ": " << line << ": ";
        cerr << s;
        cerr << endl;

    exit( 1 );
}

void
warning( String s, String file, int line )
{
    cerr << endl;
    if ( file != "" && file != "0" )
        cerr << file << ": " << line << ": warning: ";
    cerr << s;
    cerr << endl;
}
