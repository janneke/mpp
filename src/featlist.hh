#ifndef __FEATURELIST_H
#define __FEATURELIST_H

#include "glist.hh"

#include "feature.hh"


declare( List, Feature );

#ifndef FeatureList
#define FeatureList GENERIC( Feature, List )
#define FeatureListIterator GENERIC( Feature, ListIterator )
#endif

#if 0
/****************************************************************************
  class FeatureList
--*/

struct FeatureList : public genericFeatureList
{
//    static FeatureList& getFeatureList( istream& is );
    FeatureList();
    FeatureList( istream& is );
    ~FeatureList();
};

inline FeatureList::FeatureList() :
    genericFeatureList()
{
}

inline FeatureList::~FeatureList()
{
}
//-- class FeatureList //
#endif

#endif //__FEATURELIST_H //
