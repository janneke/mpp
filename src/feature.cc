/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : feature.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include <ctype.h>
#include "simpnote.hh"
#include "beam.hh"
#include "notename.hh"
#include "feature.hh"
#include "script.hh"
#include "strlist.hh"

#include "init.hh"
#include "staff.hh"
#include "mpp.hh"
#include "string.hh"

/****************************************************************************
  class Feature
--*/

void
Feature::invokeCalculate( Object& feature, void* note )
{
    ( (Feature&)feature ).calculate( *(SimpleNote*)note );
}

Feature::Feature( Feature& feature ) :
    Macro ( feature.name, feature.substitute, feature.parameterCount ),
    note( feature.note ),
    type( feature.type )
{
}

Feature::Feature( SimpleNote& n, Type t ) :
    Macro ( "" ),
    note( n ),
    type( t )
{
}

Feature::Feature( String name, String substitute, Type t, int parameterCoun ) :
    Macro ( name, substitute, parameterCoun ),
    note( NOSIMPLENOTE ),
    type( t )
{
}

Feature::Feature( Staff& staff, SimpleNote& n ) :
    Macro ( "" ),
    note( n )
{
    getFrom( staff );
}

Feature::~Feature()
{
    ;// monitor << "~Feature" << endl;
}

void
Feature::operator =( Feature& feature )
{
    type = feature.type;
    substitute = feature.substitute;
}

void
Feature::execute( SimpleNote& )
{
}

void
Feature::calculate( SimpleNote& )
{
}

Feature& 
Feature::getFeature(Staff& staff, SimpleNote& note)
{

//    ^{.}   -> \ust<pitch>          so . -> staccato
//    _{v}   -> \lsp<pitch>             v -> spicato
//    ^{>}   -> \usf<pitch>          so > -> sforzando
//    ^{1}   -> \ufinger1<pitch>        0 -> finger
//    _{\ff} -> \lxxx\ff<pitch>         \ -> text

    if (staff.is->eof())
	return NOFEATURE;
    
    char c = staff.is->peek();
    switch ( c )
        {
        case '^'  :
            return *new Script( staff, note, Token::UP );
        case '_'  :
            return *new Script( staff, note, Token::DOWN );
        case '-'  :
            return *new Script( staff, note );
        case '\\' :
            {
            Feature feature( staff, note );
            if ( feature.type != FEATURE )
                return *new Script( feature );
            else
                return *new Feature( feature );
            }
//        case ' '  :
//        case '\t' :
//        case '\n' :
#ifdef __OLDSLURS__
        case ')'  :
#else
        case '('  :
#endif
        case ']'  :
        case '}'  :
        case ':'  :
        case '|'  :
        case '!'  :
            return NOFEATURE;
        default:
            if ( !isspace( c ) )
                staff.error( quoteString( "invalid feature", c ) );
            return NOFEATURE;
        }
}

int
Feature::getFrom( Staff& staff )
{
//    if ( note != NONOTE )
//    if ( &note != ZEROSIMPLENOTE )
        {
        StringList parameterList;
        Parameter parameter( staff );
        parameterList.put( *new String( parameter.substitute ) );
        execute( parameterList, staff );
        }
    return 0;
}

void
Feature::execute( StringList& parameterList, Staff& staff )
{
    ;// monitor << "Feature::getFrom" << flush;

    String s = parameterList.top();

    if ( isdigit( s[ 0 ] ) && ( s[ 1 ] == '\0' ) )
        {
//        substitute = String ( "finger{" ) + s + '}';
        substitute = "finger{";
        substitute += s;
        substitute += '}';
        type = FINGER;
        }
    else
        {


//        if ( (feature = &(Feature&)featureList.firstFeature( 
	// Token::compare, (void*)(const char*)s ) ) == ZERO )

//       Feature *feature = &(Feature&)  //	    featureList.firstFeature( Token::compare, (void*) &s );	

	Feature *feature = find_feature(s);
	

        if (feature == ZERO )
            {
            staff.warning( quoteString( "feature not found", s ) );
            substitute = "{\\medtype ";
            substitute += s;
            substitute += "}";
            type = STAFF;
            }
        else
            {
            feature->execute( note );
            *this = *feature;
            if ( substitute.len() )
                if ( type == STAFF )
                    {
                    substitute = "{\\directstyle ";
                    substitute += feature->substitute;
                    substitute += "}";
                    }
            }
        }
}


void
Feature::printOn( ostream& ) const
{
#if 0
    if ( !substitute.len() )
        return;

    os << substitute;
    os << '{' << NoteName( pitch ) << '}';
#endif
}
//-- class Feature //
