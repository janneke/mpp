// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : feature.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __FEATURE_H
#define __FEATURE_H

#include "globals.hh"
#include "macro.hh"

/****************************************************************************
  class Feature
--*/

class Feature : public Macro
{
 public:
    enum Type
    {
        FEATURE = -1, ACCENT, CRESCENDO, FINGER, SLUR, SPACE, STAFF, TEXT
    };

 protected:
    SimpleNote& note;

 public:
    Type type;

    virtual void operator =( Feature& feature );

    static void invokeCalculate( Object& feature, void* note );
    static Feature& getFeature( Staff& staff, SimpleNote& n );

    virtual void calculate( SimpleNote& note );
    virtual void execute( StringList& parameterList, Staff& staff );
    virtual void execute( SimpleNote& note );
    virtual int getFrom( Staff& staff );
    virtual void printOn( ostream& os ) const;

    Feature( Feature& feature );
    Feature( Staff& staff, SimpleNote& n );
    Feature( SimpleNote& n, Type t = FEATURE );
    Feature( String name, String substitute = "", Type t = FEATURE, int parameterCount = 0 );
    virtual ~Feature();
};

#define ZEROFEATURE ((Feature*)ZERO)
#define NOFEATURE (*ZEROFEATURE)
//-- Feature //




Feature *find_feature(String s);



#endif // __FEATURE_H //



