#ifndef __GARRAY_H
#define __GARRAY_H

#include "array.hh"



#include "generic.hh"


#define gArray( Type )          GENERIC( Type, Array )
#define gArrayIterator( Type )  GENERIC( Type, ArrayIterator )
#define Cast( Type )           (Type&)

                               // only when using base Array of void*
                               // ( or TURBO C )
#define CastO                  (Object&)

#define Arraydeclare( Type )                                                 \
struct gArray( Type ) : public Array                                         \
{                                                                            \
    Type& operator []( int index ) const                                     \
                               { return Cast( Type )                         \
                                      Array::operator []( index ); }         \
    Type& get()                { return Cast( Type )Array::get(); }          \
    Type& get( int index )     { return Cast( Type )Array::get( index ); }   \
    int put( Type& object )    { return Array::put( CastO object ); }        \
    int put( Type& object, int index )                                       \
                               { return Array::put( CastO object, index ); } \
    Type& _name2( first, Type )( ITERATE_TEST test, void* parameters )       \
                               { return Cast( Type )                         \
                                   Array::first( test, parameters ); }       \
									     \
    gArray( Type )( int top, int bottom = 0, int delta = 0 ) :               \
        Array( top, bottom, delta )                                          \
                               { }       	                             \
    virtual ~gArray( Type )()  { }                                           \
};                                                                           \
                                                                             \
struct gArrayIterator( Type ) : public ArrayIterator                         \
{                                                                            \
    Type& operator ()()        { return Cast( Type )                         \
                                   ( (ArrayIterator&)*this )(); }            \
    Type& operator ++( POSTFIX_INT )                                         \
                               { return Cast( Type )                         \
                                   ( (ArrayIterator&)*this )++; }            \
    gArrayIterator( Type )( gArray( Type )& array ) :                        \
        ArrayIterator( array ) { }                                           \
};

#endif //__GARRAY_H //
