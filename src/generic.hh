// what's this for??

#ifndef __GENERIC_H
#define __GENERIC_H

                               // macro arguments are not expanded 
                               // ANSI requires an extra level of indirection
#define _name2(z, y)		_name2_x(z, y)
#define _name2_x(z, y)		z##y
#define _name3(z, y, x)	        _name3_x(z, y, x)
#define _name3_x(z, y, x)	z##y##x
#define _name4(z, y, x, w)	_name4_x(z, y, x, w)
#define _name4_x(z, y, x, w)	z##y##x##w

#define declare(z, y) _name2(z, declare)(y)
#define implement(z, y) _name2(z, implement)(y)
#define declare2(z, y, x) _name2(z, declare2)(y, x)
#define implement2(z, y, x) _name2(z, implement2)(y, x)
#include "cplusplu.hh"

#if IDENTIFIER_MAX < 64
#define GENERIC( z, y )        _name3_x( g, z, y )
#define READABLE( z )          _name2_x( r, z )
#else
#define GENERIC( z, y )        _name3_x( generic, z, y )
#define READABLE( z )          _name2_x( Readable, z )
#endif

#endif // __GENERIC_H //
