// glist -- generic list using #define. Hack for old C++ compilers

// should use templates.

#ifndef __GLIST_H
#define __GLIST_H

#include "list.hh"
#include "generic.hh"

#define gList( Type )          GENERIC( Type, List )
#define gListIterator( Type )  GENERIC( Type, ListIterator )

#define Cast( Type )           (Type&)
                               // only when using base List of void*
                               // ( or TURBO C )
#define CastO                  (Object&)

#define Listdeclare( Type )                                                  \
struct gList( Type ) : public List                                           \
{                                                                            \
    Type& bottom()             { return Cast( Type )List::bottom(); }        \
    Type& get()                { return Cast( Type )List::get(); }           \
    int insert( Type& object ) { return List::insert( object ); }            \
    Type& next()               { return Cast( Type )List::next(); }          \
    int put( Type& object )    { return List::put( CastO object ); }         \
    int putList( gList( Type )& list )                                       \
                               { return List::putList( list ); }             \
    Type& remove()             { return Cast( Type )List::remove(); }        \
    Type& top()                { return Cast( Type )List::top(); }           \
    Type& _name2( first, Type )( ITERATE_TEST test, void* parameters )       \
                               { return Cast( Type )                         \
                                   List::first( test, parameters ); }        \
									     \
    gList( Type )() : List()   { }       	                             \
    gList( Type )( gList( Type )& list ) :    	                             \
        List( list )           { }					     \
    virtual ~gList( Type )()   { /*clear();*/ }                              \
};                                                                           \
                                                                             \
struct gListIterator( Type ) : public ListIterator                           \
{                                                                            \
    Type& operator ()()        { return Cast( Type )                         \
                                   ( (ListIterator&)*this )(); }             \
    Type& operator ++( POSTFIX_INT )                                         \
                               { return Cast( Type )                         \
                                   ( ( (ListIterator&)*this )++ ); }         \
    gListIterator( Type )( gList( Type )& list ) :                           \
        ListIterator( list )   { }                                           \
};

#endif //__GLIST_H //
