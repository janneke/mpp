#ifndef __GLOBALS_H
#define __GLOBALS_H

#include <assert.h>
#define CENTRAL_OBJECT // for string.hh

#include "config.hh"

#ifdef HAVE_NO_BOOL
/* 
  sorry hw, won't work with operators
  enum  bool { false, true }; 
  and must be defined before including string.hh!
 */
  #define false 0
  #define true 1
  typedef int bool;
#endif

#include "string.hh"
#include "proto.hh"

#ifdef CENTRAL_OBJECT
#include "object.hh"
#endif

#define max(a,b)        ( ( ( a ) > ( b ) ) ? (a) : ( b ) )
#define min(a,b)        ( ( ( a ) < ( b ) ) ? (a) : ( b ) )

#ifdef HAVE_NO_ABS
#define abs(a)          ( ( ( a ) > 0 ) ? ( a ) : ( -( a ) ) )
#else
#include <stdlib.h>
#endif
#define sign(a)         ( ( ( a ) > 0 ) ? ( 1 ) : ( -1 ) )

extern ostream *mout, *mlog;


#define STAFF_MAX 9  /* Musi*TeX maximum nr of staffs */


#endif
