// -*-C++-*-
#ifndef __GRLIST_H
#define __GRLIST_H

#include "glist.hh"
#include "macro.hh"


declare( List, Macro );

/****************************************************************************
  class genericReadableList
--*/

#define genericMacroList       GENERIC( Macro, List )
#define genericMacroListIterator GENERIC( Macro, ListIterator )
#define gReadableList( Type )  GENERIC( Type, READABLE( List ) )
#define gReadableListIterator( Type )  GENERIC( Type, READABLE( ListIterator ) )

#define getReadable( Type )   _name2( get, Type )
#define Cast( Type )           (Type&)

#define ReadableListdeclare( Type )                                          \
struct gReadableList( Type ) : public genericMacroList                       \
{                                                                            \
    gReadableList( Type )() :                                                \
        genericMacroList()     { }                                           \
    gReadableList( Type )( gReadableList( Type )& list ) :                   \
        genericMacroList( list ){ }                                          \
    gReadableList( Type )( Staff& staff );                                   \
    virtual ~gReadableList( Type )() { }                                     \
};                                                                           \
                                                                             \
struct gReadableListIterator( Type ) : public genericMacroListIterator       \
{                                                                            \
    gReadableListIterator( Type )( gReadableList( Type )& list ) :           \
        genericMacroListIterator( list ) { }                                 \
};

#define ReadableListimplement( Type )                                        \
gReadableList( Type )::gReadableList( Type )( Staff& staff ) :               \
    genericMacroList()                                                       \
{                                                                            \
    Type* type;                                                              \
    while ( *( type = &Type::getReadable( Type )( staff ) ) != NOOBJECT )    \
        put( *type );                                                        \
}

//-- class genericReadableList //

#endif //__GRLIST_H //
