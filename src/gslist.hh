#ifndef __GSLIST_H
#define __GSLIST_H


#include "list.hh"
#include "sortable.hh"
#include "generic.hh"


#define gSortedList( Type )    GENERIC( Type, SortedList )
#define gSortedListIterator( Type )  GENERIC( Type, SortedListIterator )

#define Cast( Type )           (Type&)
#define CastO                  (Object&)

#define SortedListdeclare( Type )                                            \
class gSortedList( Type ) : public List                                      \
{                                                                            \
    const int sortOrder;                                                     \
    Type& next()               { return Cast( Type )List::next(); }          \
    int putHighToLow( Type& sortable );                                      \
    int putLowToHigh( Type& sortable );                                      \
 public:								     \
    Type& bottom()             { return Cast( Type )List::bottom(); }        \
    Type& get()                { return Cast( Type )List::get(); }           \
    int put( Type& sortable );                                               \
    Type& top()                { return Cast( Type )List::top(); }           \
    Type& _name2( first, Type )( ITERATE_TEST test, void* parameters )       \
                               { return Cast( Type )                         \
                                   List::first( test, parameters ); }        \
                                                                             \
    gSortedList( Type )( int Order = 0 ) :                                   \
        sortOrder( Order )     { }                                           \
    virtual ~gSortedList( Type )()                                           \
                               { /*clear();*/ }                              \
};                                                                           \
                                                                             \
struct gSortedListIterator( Type ) : public ListIterator                     \
{                                                                            \
    Type& operator ()()        { return Cast( Type )                         \
                                   ( (ListIterator&)*this )(); }             \
    Type& operator ++( POSTFIX_INT )                                         \
                               { return Cast( Type )                         \
                                   ( ( (ListIterator&)*this )++ ); }         \
    gSortedListIterator( Type )( gSortedList( Type )& list ) :               \
        ListIterator( list )   { }                                           \
};

#define SortedListputimplement( Type )                                       \
int gSortedList( Type )::put( Type& sortable )                               \
{                                                                            \
   return sortOrder ? putHighToLow( sortable ) : putLowToHigh( sortable );   \
}                                                                            \
                                                                             \
int gSortedList( Type )::putLowToHigh( Type& sortable )                      \
{                                                                            \
    if ( empty() )                                                           \
        return List::insert( CastO sortable );                               \
    Type& oldtop = top();                                                    \
    Type& oldbottom = bottom();                                              \
    if ( sortable > bottom() )                                               \
        return List::put( CastO sortable );                                  \
    while ( sortable > top() )                                               \
       if ( oldbottom == next() )                                            \
           break;                                                            \
    int ret = List::insert( CastO sortable );                                \
    if ( sortable > oldtop )                                                 \
        while ( oldtop != next() );                                          \
    return ret;                                                              \
}                                                                            \
                                                                             \
int gSortedList( Type )::putHighToLow( Type& sortable )                      \
{                                                                            \
    if ( empty() )                                                           \
        return List::insert( CastO sortable );                               \
    Type& oldtop = top();                                                    \
    Type& oldbottom = bottom();                                              \
    if ( sortable < bottom() )                                               \
        return List::put( CastO sortable );                                  \
    while ( sortable < top() )                                               \
       if ( oldbottom == next() )                                            \
           break;                                                            \
    int ret = List::insert( CastO sortable );                                \
    if ( sortable < oldtop )                                                 \
        while ( oldtop != next() );                                          \
    return ret;                                                              \
}


#endif // __GSLIST_H  //
