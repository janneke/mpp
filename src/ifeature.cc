/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : ifeature.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include "feature.hh"
#include "script.hh"
#include "stem.hh"
#include "init.hh"
#include "mpp.hh"
#include "textstr.hh"
#include "path.hh"

#define SCRIPT_INIT "script.ini"

#include "featlist.hh"
FeatureList featureList;


Feature *find_feature(String s)
{
    return &(Feature&)  
	featureList.firstFeature( Token::compare, (void*) &s );	        \
}

    
NESTED_IN( Feature )Type
char2FeatureType( String type)
{
    if ( type == "ACCENT" )
        return Feature::ACCENT;
//    else if ( type == "STAFF" )
//        return Feature::STAFF;
    else if ( type == "TEXT" )
        return Feature::TEXT;
    else
        return Feature::STAFF;
}

NESTED_IN( Token )Orientation 
char2TokenOrientation( String orientation)
{
    if ( orientation == "UP" )
        return Token::UP;
//    else if ( orientation == "DOWN" )
//        return Token::DOWN;
    else if ( orientation == "UNDEFINED" )
        return Token::UNDEFINED;
    else
        return Token::DOWN;
}

void
initFeatureList()
{
     String initName = search_path->find(SCRIPT_INIT);
     
     filed_stringlist scripts( initName );

     while ( scripts )
	 {
	 Text_record script = scripts++;
	 
	 if(script.sz() == 4) 
	     {
	     
	     String name = script[0];
	     String substitute = script[1];
	     
	     NESTED_IN( Feature )Type type = char2FeatureType( script[2] );
	     NESTED_IN( Token )Orientation orientation = 
		 char2TokenOrientation( script[3] );
	     featureList.put( *new Script( 
		 name, substitute, type, orientation ) );
	     }
	 else if (script.sz() != 0)
	     scripts.error("format");	 
        }



    featureList.put( *new Script( " ", "gobble", Feature::SPACE ) );
    featureList.put( *new Script( "(#)", "csh", Feature::ACCENT ) );
    featureList.put( *new Script( "xplet", "xplet", 
				  Feature::TEXT, Token::UNDEFINED, 1 ) );

    //barf
    #include "feature.ini"
}

//****************************************************************
// OBSOLETE

#if 0
#ifndef NO_RUNTIME
#define FEATURES_RUNTIME
#endif

#ifdef FEATURES_RUNTIME


#include "textdata.hh"

NESTED_IN( Feature )Type char2FeatureType( String s )
{
    String type( s );
    if ( type == "ACCENT" )
        return Feature::ACCENT;
//    else if ( type == "STAFF" )
//        return Feature::STAFF;
    else if ( type == "TEXT" )
        return Feature::TEXT;
    else
        return Feature::STAFF;
}

NESTED_IN( Token )Orientation char2TokenOrientation( String s )
{
    String orientation( s );
    if ( orientation == "UP" )
        return Token::UP;
//    else if ( orientation == "DOWN" )
//        return Token::DOWN;
    else if ( orientation == "UNDEFINED" )
        return Token::UNDEFINED;
    else
        return Token::DOWN;
}
#include "textstr.hh"
void
initFeatureList()
{
     String initName = search_path->find( SCRIPT_INIT);
     filed_stringlist scripts( initName );

     while ( scripts )
	 {
	 stringlist script = scripts++;

	 if(script.sz() == 4) 
	     {
	     
	     String name = script[0];
	     String substitute = script[1];
	     
	     NESTED_IN( Feature )Type type = char2FeatureType( script[2] );
	     NESTED_IN( Token )Orientation orientation = 
		 char2TokenOrientation( script[3] );
	     featureList.put( *new Script( 
		 name, substitute, type, orientation ) );
	     }
	 else if (script.sz() != 0)
	     scripts.error("format");
	 
        }

    featureList.put( *new Script( " ", "gobble", Feature::SPACE ) );
    featureList.put( *new Script( "(#)", "csh", Feature::ACCENT ) );
    featureList.put( *new Script( "xplet", "xplet", 
				  Feature::TEXT, Token::UNDEFINED, 1 ) );


    #include "feature.ini"
}

#else // not FEATURES_RUNTIME //

void
initFeatureList()
{
    ;// monitor << "initFeatureList" << endl;
    #include "feature.ini"
    #include "scriptcc.ini"
}

#endif // not FEATURES_RUNTIME //
#endif 
