/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : imacro.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include "notename.hh"
#include "note.hh"
#include "clef.hh"
#include "key.hh"
#include "verb.hh"
#include "slope.hh"
#include "define.hh"
#include "imacro.hh"
#include "init.hh"
//#include "mpp.hh"

MacroList macroList;

void
initMacroList()
{
    ;// monitor << "initmacroList" << endl;

    #include "key.ini"

    #include "clef.ini"

    #include "bar.ini"

    #include "macro.ini"
}

