// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : imacro.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __IMACRO_H
#define __IMACRO_H

#include "maclist.hh"


extern MacroList macroList;

#endif //__IMACRO_H //
