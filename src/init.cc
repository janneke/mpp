/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : init.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#ifdef __TURBOC__              // _stklen
#include <dos.h>               // _stklen
unsigned _stklen  = 8192;
#endif

#include <fstream.h>
#include "globals.hh"
#include "init.hh"
#include "mpp.hh"

ofstream _log( "m.log" );

#ifdef __unix__
//ofstream _cnull( "/dev/null" );
ofstream _cnull( "null" );
#else
ofstream _cnull( "nul" );       // msdos
#endif

ostream& cnull = _cnull;
ostream& monitor = _log;
 

