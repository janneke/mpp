/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : init.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __INIT_H
#define __INIT_H

#include "globals.hh"

#ifdef __unix__
extern ostream& my_monitor;
#define monitor my_monitor
#else
extern ostream& monitor;
#endif

extern ostream& cnull;

#endif // __INIT_H //
