/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : inote.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include "init.hh"
#include "inote.hh"
#include "path.hh"

#define NOTE_INIT "note.ini"

// major bug fix: -1
NoteNameArray noteNames( ( 7 + 1 ) * 5  - 1 ); 
// 7 = #no. notes in eigth, 5 = flatflat... sharpsharp
// and - 1, because array starts default at index 0
// several routines assume noteNames.size() % 5 == 0 !!


#include "textstr.hh"


void
initNoteNames()
{
    String initName = search_path->find( NOTE_INIT);
    filed_stringlist notes( initName );

    while ( notes )
        {
        Text_record note = notes++;
	if (note.sz() == 4) 
	    {
	    
	    String name = note[0];
	    int pitch = note[1].value();
	    int height = note[2].value();
	    int offset =  note[3].value();
	    noteNames.put( *new NoteName( name, pitch, height, offset ) );
	    }
	else if (note.sz() != 0)
	    notes.error("format");
	}
}
