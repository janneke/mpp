// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : inote.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __INOTE_H
#define __INOTE_H

void initNoteNames();


#include "namearra.hh"


extern NoteNameArray noteNames;

#endif //__INOTE_H //
