// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : integer.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __INTEGER_H
#define __INTEGER_H

#include "object.hh"


class ostream;

class Integer : public Object {

protected:
    int value;

public:
    operator int()             { return value; }

    virtual void printOn( ostream& os ) const
                               { os << value; }
    static void sum( Object& integer, void* sum )
    {
        *(int*)sum += ( (Integer&)integer ).value;
    }

    Integer( int i )           { value = i; }     
    Integer( Integer& integer ){ value = (int)integer; }
    virtual ~Integer()         { }
};

#endif // __INTEGER_H //
