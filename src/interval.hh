// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : interval.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __INTERVAL_H
#define __INTERVAL_H

/****************************************************************************
  class Interval
--*/

struct Interval {
    int begin;
    int length;

    Interval( int b = 0, int l = 0 )   
                               { begin = b; length = l; }
    virtual ~Interval()        {  }
};
//-- class Interval //

#endif // __INTERVAL_H //
