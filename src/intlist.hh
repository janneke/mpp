#ifndef __INTEGERLIST_H
#define __INTEGERLIST_H

#include "glist.hh"
#include "integer.hh"


declare( List, Integer );
#ifndef IntegerList
#define IntegerList GENERIC( Integer, List )
#endif
#define IntegerListIterator GENERIC( Integer, ListIterator )

#endif //__INTEGERLIST_H //
