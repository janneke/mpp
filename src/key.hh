// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : key.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __KEY_H
#define __KEY_H

#include "macro.hh"
#include "notename.hh"         // Sign


/****************************************************************************
  class Key
--*/

class Key : public Macro 
{
 friend class Transpose;       // to read baseSubstitute
 
protected:
    int flats;
    int sharps;

    void resetAccidentals();
    void setAccidental( String name, NESTED_IN( NoteName )Sign sign );
    void setAccidentals();
    void transposeAccidentals( int transpose );
    Key& transposeKey( int transpose, int transposeHeight );
    
public:
    virtual void execute( StringList& parameters, Staff& staff );
    void sustain( int pitch, NESTED_IN( NoteName )Sign sign );

    Key( String s, int fl, int sh );
    virtual ~Key();
};

#define ZEROKEY ((Key*)ZERO)
#define NOKEY   (*ZEROKEY)

//-- class Key //

/****************************************************************************
  class Transpose
--*/

class Transpose : public Macro
{
 public:
    virtual void execute( StringList& parameters, Staff& staff );

    Transpose( String name );
    virtual ~Transpose();
};
//-- class Transpose //

#endif // __KEY_H //
