// -*-C++-*-
/****************************************************************************
  PROJECT: FlowerSoft C++ library
  FILE   : list.h
--*/

#ifndef __LIST_H
#define __LIST_H

#include "object.hh"


/****************************************************************************
  class Link
--*/

class Link : public Object 
{
 friend class List;
 friend class ListIterator;
 friend class ConstListIterator;

 protected:
    Link* next;
    Object& object;

    virtual void printOn( ostream& os ) const;

//    Link();
    Link( Object& o, Link& n );
    virtual ~Link();
};

#define ZEROLINK ((Link*)ZERO)
#define NOLINK (*ZEROLINK)

#if 0
inline Link::Link() : 
    object ( *this ),
    next( ZEROLINK )
{ 
}
#endif

inline Link::Link( Object& o, Link& n ) :
    next( &n ) ,
    object( o )    
{   
}

inline Link::~Link()
{ 
}

inline void Link::printOn( ostream& os ) const
{
    os << "Link";
}
//-- class Link //

/****************************************************************************
  class List
--*/

class List : public Object 
{
friend class ListIterator;
friend class ConstListIterator;

    void _clear();
protected:
    int _count;
    Link* last;

    Link& _top();

public:
    Object& bottom();
    void clear();
    int count();
    void each( ITERATE action, void* parameters = 0 );
    void each( ITERATE_CONST action, void* parameters = 0 ) const;
    int empty();
    Object& first( ITERATE_TEST test, void* parameters = 0 );
    Object& get();
    int insert( Object& object );
    Object& next();
    Object& previous();
    virtual void printOn( ostream& os ) const;
    int put( Object& object );
    int putList( List& list );
    Object& remove();
    Object& top();

    List();
    List( List& list );
    virtual ~List();
};

inline List::List() :
    _count( 0 ), 
    last( ZEROLINK )
{ 
}

inline List::List( List& list ) :
    _count( 0 ),
    last( ZEROLINK )
{
    if ( list._count )
        putList( list );
}

inline void List::clear()
{
    if ( last != ZEROLINK )
        _clear();
}
        
inline List::~List()
{
    clear(); 
}

inline Link& List::_top()
{ 
    return ( ( last != ZEROLINK ) ? *last->next : *ZEROLINK ); 
}

inline Object& List::bottom()
{ 
    return ( ( last != ZEROLINK ) ? last->object : NOOBJECT ); 
}

inline int List::count()
{ 
    return _count; 
}

inline int List::empty()           
{ 
    return ( _count == 0 );
}

inline Object& List::top()
{ 
    Link *l; 
    return ( ( ( l = &_top() ) != ZEROLINK ) ? l->object : NOOBJECT ); 
} 

#define ZEROLIST ((List*)ZERO)
#define NOLIST (*ZEROLIST)
//-- class List //

/****************************************************************************
  class ListIterator
--*/

class ListIterator : public Object 
{
    List& list;
    Link* cursor;

public:
    virtual operator int() const;
    Object& operator ()();
    Object& operator ++( POSTFIX_INT );

    virtual void printOn( ostream& os ) const;
    virtual void reset();

    ListIterator( List& l );
    virtual ~ListIterator();
};

inline ListIterator::ListIterator( List& l ) :
    list( l )
{
    cursor = &list._top();
}

inline ListIterator::~ListIterator() 
{
}

inline ListIterator::operator int() const
{ 
    return cursor != ZEROLINK; 
}

inline Object& ListIterator::operator ()() 
{
    return ( ( cursor != ZEROLINK ) ? ( cursor->object ) : NOOBJECT );
}

inline Object& ListIterator::operator ++( POSTFIX_INT )
{
    Object *object = ( ( cursor != ZEROLINK ) ? &( cursor->object ) : ZERO );
    if ( list.last != ZEROLINK )
        cursor = ( cursor != list.last ) ? cursor->next : ZEROLINK;
    return *object;
}

inline void ListIterator::reset()
{ 
    cursor = &list._top(); 
}

inline void ListIterator::printOn( ostream& os ) const
{
    os << "ListIterator";
}

//-- class ListIterator //

#endif //__LIST_H //
