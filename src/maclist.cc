/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : maclist.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include "maclist.hh"

#if 0

/****************************************************************************
  class MacroList
--*/

MacroList::MacroList( istream& is ) :
    genericMacroList()
{
    Macro* macro;
    while ( *( macro = Macro::getMacro( is ) ) != NOMACRO )
        put( *macro );
}

#else

implement( ReadableList, Macro );

#endif

//-- class MacroList //
