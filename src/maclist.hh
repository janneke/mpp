#ifndef __MACROLIST_H
#define __MACROLIST_H

#if 0
#include "glist.hh"
#include "macro.hh"

declare( List, Macro );

#define MacroList genericMacroList
#define MacroListIterator genericMacroListIterator

struct MacroList : public genericMacroList 
{
//    static MacroList& getMacroList( istream& is );
    MacroList();
    MacroList( istream& is );
    ~MacroList();
};
inline MacroList::MacroList() :
    genericMacroList()
{
}
inline MacroList::~MacroList()
{
}
#else

/****************************************************************/
#include "grlist.hh"

declare( ReadableList, Macro );

#ifndef MacroList
#define MacroList GENERIC( Macro, READABLE( List ) )
#define MacroListIterator GENERIC( Macro, READABLE( ListIterator ) )
#endif

#endif

#endif //__MACROLIST_H //
