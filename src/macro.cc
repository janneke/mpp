/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : macro.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/


#include <ctype.h>
#include "globals.hh"
#include "strlist.hh"
#include "macro.hh"
#include "imacro.hh"
#include "init.hh"
#include "staff.hh"
#include "mpp.hh"
#include "duration.hh"

/****************************************************************************
  class DefaultDuration
--*/

DefaultDuration::DefaultDuration( String s, const int p ) :
    Macro( s, "", p )
{
}

DefaultDuration::~DefaultDuration()
{
}

void
DefaultDuration::execute( StringList& parameterList, Staff& staff )
{
    ;// monitor << "DefaultDuration::execute: " << parameterList.top() << endl;
    int newxDuration = ( (String)parameterList.top() ).value();
    if (!is_note_len(newxDuration))
        staff.warning( String( "invalid duration:" ) + String( newxDuration ) );
    else
        staff.xDuration = newxDuration;
}
//-- class DefaultDuration //

/****************************************************************************
  class Macro
--*/

Macro::Macro( String name, String sub, const int parameterCount ) :
    baseSubstitute( sub ),
    substitute( sub ),
    Token( name, parameterCount )
{
//    if ( !substitute.len() )
//        baseSubstitute = name;
}

Macro::Macro( Staff& staff, String sub, const int parameterCount ) :
    baseSubstitute( sub ),
    substitute( sub ),
    Token ( "", parameterCount )
{
    getFrom( staff );
}

Macro& Macro::getMacro( Staff& staff )
{
    istream& is = *staff.is;

    WhiteSpace ws( staff );
    char c = is.peek();

    switch( c )
        {
#if 0
        case '%'  :          // comment
            return *new Comment( staff );
#endif
        case '\\' :
            return *new Macro( staff );
//        case (char)EOF:
//            staff.warning( "unexpected end of file" );
//            return NOMACRO;
        default:
//                staff.error( quoteString( "invalid macro:", c ) );
            return NOMACRO;
        }
}

Macro::~Macro()
{
}


void
Macro::execute( StringList& parameterList, Staff& )
{
    substitute = baseSubstitute;
//    substitute = name;

    StringListIterator parameters( parameterList );

    while( parameters )
        {
        substitute += "{";
        if ( isalpha( parameters()[0] ) )
            substitute += "\\";
        substitute += parameters++;
        substitute += "}";
        }
}

/*
   read a (already defined) macro from infile, with params
 */
int
Macro::getFrom( Staff& staff )
{
    istream& is = *staff.is;
    char c = staff.is->peek();
    if ( c == '\\' )
        is.get( c );

    String     s = Token::_getFrom( is );			  // get from stream

    // search global macrolist for this string
    Macro* macro=&(Macro&)macroList.firstMacro( Token::compare, (void*) &s );
    
//    if ( (macro = &(Macro&)macroList.firstMacro( Token::compare, (void*)(char const *)s ) ) == ZERO )
  
    if ( macro == ZERO )
        {
        if ( !s.len() )
            staff.error( quoteString( "macro not found", name ) );
        else
            staff.error( quoteString( "macro not found",s ) );
        }

    name = macro->name;
    parameterCount = macro->parameterCount;

    StringList parameterList;

    for ( int i= 0; i < parameterCount; i++ )
        {
        Parameter parameter( staff );
        parameterList.put( *new String(parameter.substitute) );
        }

    if ( parameterList.count() < parameterCount )
        staff.error( "parameter expected" );

    // do it.
    macro->execute( parameterList, staff );

//    if ( !substitute.len() )
        substitute = macro->substitute;

    ;// monitor << "substitute:" << name << ',' << macro->name << endl;

    return 0;
}

void
Macro::printOn( ostream& os ) const
{
    ;// monitor << "Macro::printOn" << endl;
    os << substitute;
}

int
Macro::validCharacter( const char c ) const
{
    return ( Token::validCharacter( c ) && isalpha( c ) );
}
//-- class Macro //

/****************************************************************************
  class Parameter
--*/

Parameter::Parameter( Staff& staff )
{
    WhiteSpace ws( staff );
    getFrom( staff );
}

Parameter::~Parameter()
{
}

Parameter::operator String() const
{
    return substitute;
}

String
Parameter::_getFrom( istream& is )
{
    char c = is.peek();
//    String &s = *new String;
    String s;

    if ( c ==  '\\' )
        {
        is.get( c );
        s = Token::_getFrom( is );
        }
    else if ( c == '{' )
        {
        int nest = 1;
        is.get( c );
        c = is.peek();
        while ( nest && Token::validCharacter( c ) ) // \{ \} won't work
            {
            is.get( c );
            if ( c == '{' )
                nest++;
            else if ( c == '}' )
                nest--;
            s += c;
            c = is.peek();
            }
                               // strip braces
//         s = s.mid( 2, s.len() - 2 );
         s = s.left( s.len() - 1 );
        }
    else
        {
        is.get( c );
        ;// monitor << c << ':';
        s = String( c );
        ;// monitor << s << endl;
        }

    return s;
}

int
Parameter::getFrom( Staff& staff )
{
    substitute = _getFrom( *staff.is );
    return 0;
}

int
Parameter::validCharacter( const char c ) const
{
    return ( Token::validCharacter( c ) && isalpha( c ) );
}
//-- class Parameter //
