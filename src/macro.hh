// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : macro.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __MACRO_H
#define __MACRO_H

#include "token.hh"

class Staff;

/****************************************************************************
  class Macro
--*/

class Macro : public Token 
{
friend class Token;

protected:
//    const char* baseSubstitute;
    String  baseSubstitute;
      /* replace macro by substitution, constant */
      // why not string?
    
    virtual int Macro::getFrom( Staff& staff );
    virtual int validCharacter( const char c ) const;

public:
    String substitute;				  /* created by execute() with (usually) use of basesubstitute  */

    static Macro& getMacro( Staff& staff );

//    virtual operator const char *() const;  

    virtual void execute( StringList& parameterList, Staff& staff );
    virtual void printOn( ostream& os ) const;

//    Macro( const char* name = "", const char* sub = "", const int parameterCount = 0 );
    Macro( String name = "", String sub = "", const int parameterCount = 0 );
//    Macro( Staff& staff, const char* sub = "", const int parameterCount = 0 );
    Macro( Staff& staff, String sub = "", const int parameterCount = 0 );
    virtual ~Macro();
};

#define ZEROMACRO ((Macro*)ZERO)
#define NOMACRO (*ZEROMACRO)
//-- Macro //


/****************************************************************************
  class Parameter
--*/

class Parameter : public Macro 
{
protected:
//    virtual String& _getFrom( istream& is );
//    virtual String _getFrom( istream& is );
    String _getFrom( istream& is );
    virtual int getFrom( Staff& staff );
    virtual int validCharacter( const char c ) const;

public:
    virtual operator String() const;  

    Parameter( Staff& staff );
    virtual ~Parameter();
};
//-- class Parameter //

/****************************************************************************
  class DefaultDuration
--*/

class DefaultDuration : public Macro 
{
public:
    virtual void execute( StringList& parameterList, Staff& staff );

//    DefaultDuration( const char* s, const int p = 0 );
    DefaultDuration( String s, const int p = 0 );
    virtual ~DefaultDuration();
};
//-- class DefaultDuration //

/****************************************************************************
  class Initiator
--*/

#define Initiator Macro
#define ZEROINITIATOR ((Initiator*)ZERO)
#define NOINITIATOR (*ZEROINITIATOR)
//-- Initiator //

/****************************************************************************
  class Terminator
--*/

#define Terminator Macro
#define ZEROTERMINATOR ((Terminator*)ZERO)
#define NOTERMINATOR (*ZEROTERMINATOR)
//-- Terminator //

#endif // __MACRO_H //
