/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : mpp.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997

  hwn, 9/95: split_path, destill_inname.
--*/

#include <iostream.h>
#include <ctype.h>
#include "globals.hh"
#include "score.hh"
#include "init.hh"
#include "mpp.hh"
#include "version.hh"
#include "lgetopt.hh"
#include "path.hh"

static void displayVersion();
static void displayHelp();
static void displayUsage();
static void identify();

ostream *mout =0, *mlog = &cout;


Mode mode = NORMAL;
File_path *search_path;

    String inName;

long_option_init the_opts[] = 
    {
        {false,"help",'h'},
	{false, "extract", 'e' },
	{true, "outfile", 'o'},
	{false,	"silent", 's' },
	{false, "quiet", 's'},
	{false, "verbose", 'v'},
	{false, "version", 'V'},
	{true, "include", 'I'},       
	{0,0}
	
    };

String outName;

void
parse_opts( Getopt_long &parser )
{
    
    /****************
      OPTIONS
     ****************/

    while (1) 
	{
	long_option_init *lop = parser();
	if (!lop)
	    break;
	
	char c = lop->shortname;
			String incdir;
	switch(c) 
	    {
	    case 'h':
		displayHelp();
	         break;
	    
	    case 'V':
		displayVersion();
		exit(0);	    
	         break;
	    case 'I':		
	 incdir = parser.optarg;
		search_path->add(incdir);
		break;
	    case 'o':
		outName = parser.optarg;
		break;
	    case 'e':
		mode = Mode( mode | EXTRACT );
		break;
	    case 's':
		mlog = &cnull;
		mode = Mode( mode | SILENT );
		break;
	    case 'v':
		warning( "verbose mode not implemented");
		mode = Mode( mode | VERBOSE );
	    default:
		assert(false);
		break;
	    }
}


    /****************
      SET INPUT
     ****************/
    if ( parser.optind < parser.argc)
        {
        inName = parser.argv[parser.optind++];
        if ( !outName.len() )
            outName = inName;
	}

    if ( !outName.len()	)
        displayUsage();

    if (parser.optind < parser.argc && !inName.len() )
        displayUsage();

    if ( outName != "-" )
        {
	String a,b,c,d;
	split_path(outName,a,b,c,d);

	outName = a+b+c+".tex";
        } 
    else 
	{
	outName = "";
	}
    
}


int
main( int argc, char *argv[])
{
    search_path = new File_path(argv[0]);
    Getopt_long parser( argc, argv, the_opts );
    parse_opts(parser);
    
    /****************
      INIT
     ****************/

    identify();                // now that everything seems ok

    // init lists first; Staff uses lists
    initNoteNames();           // init noteNames first; Macros use noteNames
    initFeatureList();
    initMacroList();

    *mlog << endl;

    /****************
      DO INPUT
     ****************/

    destill_inname(inName);
    int staffCount = 0; //???
    Score score;
    score.insert( *new Staff( inName, staffCount) );

    while ( parser.optind < argc )
        {
        String musicFile = argv[parser.optind++];

        if ( musicFile == "," )
            {
            staffCount++;
            if ( parser.optind >= argc )
                error( "second staff-file expected");
            musicFile = argv[parser.optind++];
            }

	destill_inname(musicFile);
	
	// reverse order.
	// Add a staff.
        Staff& staff = *new Staff( musicFile, staffCount );        
	score.insert( staff );

        staffCount = 0; // huh?
        }
    assert ( !score.empty() );
    
    // do it.
    if (inName == outName)
	error("Input and output are the same file");
    score.setOutput(outName);    
    score.process();        
    return  0;
    
}

/****************
  HELP!
 ****************/

const char* const USAGE_STRING =
"Usage: mpp [options] instrument1 [, staff2] [instrument2 [, ...] ...]\n";
const char *const SHORT_HELP = "use mpp --help for more information. \n";

const char* const OPTION_STRING =\
"Options:\n"
"  -                          read from standard input\n"
"  -e,      --extract         extract mode; \"\\startextract\" iso \"\\startpiece\"\n"
"  -h,      --help            print this message\n"
"  -o FILE, --outfile=FILE    write output to FILE; stdout: FILE == \"-\"\n"
"  -s,      --silent          silent mode; don't echo to stdout\n"
"  -v,      --verbose         verbose mode\n"
"  -I,      --include         add include directory\n"
"           --version         print version number and exit";



static void
displayVersion()
{
    cout << "mpp " << VERSION_STRING << endl;
}

static void
displayHelp()
{
    displayVersion();
    cout << USAGE_STRING << endl;
    cout << endl;
    cout << OPTION_STRING;
    error( "", 0, 0 );
}

static void
displayUsage()
{
    cerr << USAGE_STRING<<endl;
    cerr << SHORT_HELP;
    error( "", 0, 0 ); 
}

static void
identify()
{
    *mlog << "This is mpp " << VERSION_STRING << endl;
}
