// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : mpp.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __MPP_H
#define __MPP_H
#include "string.hh"
enum Mode 
    {
    NORMAL, EXTRACT = 1, SILENT = 2, VERBOSE = 4
    };
extern Mode mode;


void initFeatureList();
void initNoteNames();
void initMacroList();

#endif // __MPP_H //
