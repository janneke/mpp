#ifndef __NOTENAMEARRAY_H
#define __NOTENAMEARRAY_H
#include "garray.hh"
#include "notename.hh"

declare( Array, NoteName );
#ifndef NoteNameArray
#define NoteNameArray GENERIC( NoteName, Array )
#endif
#define NoteNameArrayIterator GENERIC( NoteName, ArrayIterator )

#endif //__NOTENAMEARRAY_H //
