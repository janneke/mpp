/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : note.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include "beamlist.hh"
#include "maclist.hh"
#include "slurlist.hh"
#include "simpnote.hh"
#include "noteslis.hh"
//#include "snotelis.hh"
#include "chord.hh"
#include "init.hh"
#include "staff.hh"
#include "mpp.hh"

//#define TRIPLET 1

/****************************************************************************
  class Note
--*/

int Note::stemLength = 6;

Note::Note( const Note& note ) :
    staff( note.staff ),
    chord( note.chord ),
    macroList( *new MacroList( note.macroList ) )
{
}

Note::Note( Staff& s ) :
    staff( s ),
    chord( false ),
    macroList( *new MacroList() )
{
}

int
Note::operator ==( const Sortable& test ) const
{
    return ( pitch() == ( (Note&)test ).pitch() );
}

int
Note::operator >( const Sortable& test ) const
{
    return ( pitch() > ( (Note&)test ).pitch() );
}

Note& Note::getNote( Staff& staff, MacroList& macroList )
{
    Note* note;
    SimpleNote* simpleNote;

    staff.noteCount++;

    BeamList beamList( staff );
    MacroList putme( staff );
    macroList.putList(  putme);
    SlurList slurList( staff );
    MacroList putmetoo( staff ); 
    macroList.putList(putmetoo );

    istream& is = *staff.is;
    char c = is.peek();
    if ( c == '{' )				  // chord
        {
        Chord& chord = Chord::getChord( staff );
                               // this *should* be, the first note encounterd
        simpleNote = &chord.top();
        note = &chord;
        }
    else					  // no chord, then it is simplenote
        {
        simpleNote = &SimpleNote::getSimpleNote( staff );
        note = simpleNote;
        }

#ifdef TRIPLET                 // nasty [ 4 4 r ]/3 patch
    beamList.putList( simpleNote->beamList );
    simpleNote->beamList.putList( beamList );
#else
    simpleNote->beamList.putList( beamList );
#endif
    simpleNote->macroList.putList( macroList );
    simpleNote->slurList.putList( slurList );

    return *note;
}

Note::~Note()
{
    delete &macroList;
    ;// monitor << ".";
}

//-- class Note //
