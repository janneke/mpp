// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : note.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __NOTE_H
#define __NOTE_H

#include "globals.hh"

class Macro;
class Staff;

#include "sortable.hh"
#include "generic.hh"


#ifndef MacroList
#define MacroList GENERIC( Macro, READABLE( List ) )
#endif

class MacroList;

/****************************************************************************
  class Note


  SimpleNote : Note
  BeamNote : SimpleNote
  Rest : Note
  Chord : Note     // list of contents included
  GhostNote : Note // spaced notes

--*/

class Note : public Sortable 
{
friend class Chord;
friend class StemFeature;
friend class StyleFeature;

public:

    enum StemOrientation 
    {
        DOWN = -1, UNDEFINED, UP
    };    

    enum Stem 
    { 
        NO = 0, DEFAULT, BEAM = 2 , CHORD = 4, FORCED = 8
    };

    enum Style 
    { 
        NORMAL, GRACE = 1, 
        LEFT = 2, RIGHT = 4, 
        SMALL = 8, TINY = 16
    };

    static int stemLength;

    Staff& staff;

    bool chord;
    MacroList& macroList;
    
    virtual int operator ==( const Sortable& test ) const;
       /* compare note pitch, this is virtual from baseclass */
    virtual int operator >( const Sortable& test ) const;
						  /* compare pitch */
    static void invokeCalculate( Object& note, void* )
    {
        ( (Note&)note ).calculate();
    }
    static Note& getNote( Staff& staff, MacroList& macroList );

    virtual void calculate() = 0;
    virtual int duration() = 0;
    virtual int pitch() const = 0;
    virtual void printOn( ostream& os ) const = 0;
    virtual int spacing( int additionalSpacing ) = 0;
    
protected:
    Note( const Note& note );
    Note( Staff& staff );

public:
    virtual ~Note();
};

#define ZERONOTE ((Note*)ZERO)
#define NONOTE   (*ZERONOTE)

//-- Note //

#endif // __NOTE_H //
