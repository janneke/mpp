#ifndef __NOTELIST_H
#define __NOTELIST_H
#include "glist.hh"
#include "note.hh"

declare( List, Note );
#ifndef NoteList
#define NoteList GENERIC( Note, List )
#endif
#define NoteListIterator GENERIC( Note, ListIterator )

#endif //__NOTELIST_H //
