// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : notename.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __NOTENAME_H
#define __NOTENAME_H
#include "macro.hh"


/****************************************************************************
  class NoteName
--*/

class NoteName : public Token 
{
 friend class Key;             // why if sign is public

 protected:
    Staff& staff;
    int height;
    int offset;
    int pitch;
    
 public:
    enum Sign 
    { 
        FLATFLAT = -2, FLAT, 
        NATURAL, 
        SHARP, SHARPSHARP,
        NATURALNATURAL, PLAIN, INVALID
    };
    
    Sign sign;                 // why public 
    
 protected:
    NoteName& _getFrom( istream& is );
    virtual int getFrom( Staff& staff );
    Sign getSignFrom( Staff& staff );
    virtual int validCharacter( const char c ) const;

 public:
    void operator ++();
    void operator --();
    NoteName& operator =( const NoteName& noteName );

    static int comparePitch( const Object& noteName, void* pitch );
    int getPitch() const;
    String getSign() const;
    String getSpacing() const;
    virtual void printOn( ostream& os ) const;
                               // only for Key, why public
    void sustain( Sign forcedSign );

    NoteName( int p );
    NoteName( String name );
    NoteName( String name, int p, int h, int o );
    NoteName( const NoteName& noteName );
    NoteName( istream& is );
    NoteName( Staff& s );
    virtual ~NoteName();
};

#define ZERONOTENAME ((NoteName*)ZERO)
#define NONOTENAME   (*ZERONOTENAME)

//-- class NoteName //


/****************************************************************************
  class Octave
--*/

class Octave : public Token
{
 protected:
    int octavate;
    
    virtual int validCharacter( const char c ) const;
    
 public:
    static const int central;

    operator int() const;
    virtual int getFrom( istream& is );
    virtual void printOn( ostream& ) const
                               { }

    Octave( istream& is );
    virtual ~Octave();
};

#define ZEROOCTAVE ((Octave*)ZERO)
#define NOOCTAVE   (*ZEROOCTAVE)

//-- class Octave  //

#endif // __NOTENAME_H  //
