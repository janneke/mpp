#ifndef __NOTESLIST_H
#define __NOTESLIST_H

#include "gslist.hh"
#include "simpnote.hh"


declare( SortedList, SimpleNote );

#ifndef NoteSortedList
#define NoteSortedList GENERIC( SimpleNote, SortedList )
#endif

#define NoteSortedListIterator GENERIC( SimpleNote, SortedListIterator )

#endif //__NOTESLIST_H //
