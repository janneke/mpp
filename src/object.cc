/****************************************************************************
  PROJECT: FlowerSoft C++ library
  FILE   : object.cc
--*/

#include "object.hh"

/****************************************************************************
  class Object
--*/
void
Object::dprint() 
{
    cerr << *this;
}


#ifndef __OPERATOR_NEW
#define __OPERATOR_NEW
void* Object::operator new( size_t size )
{
    void* allocated = ::operator new( size );

    if ( allocated == 0 )
        {
        warning( "out of memory", __FILE__, __LINE__ );
        return ZERO;
        }
    else
        return allocated;
}
#endif

void
Object::each( ITERATE action, void* parameters )
{
    (*action)( *this, parameters );
}

void
Object::each( ITERATE_CONST action, void* parameters ) const
{
    (*action)( *this, parameters );
}

Object& Object::first( ITERATE_TEST test, void* parameters )
{
    if ( (*test)( *this, parameters ) )
        return *this;
    else
        return NOOBJECT;
}
//-- class Object //

// #pragma startup declareErrorObject 64
ErrorObject errorObject;
