// -*-C++-*-
/****************************************************************************
  PROJECT: FlowerSoft C++ library
  FILE   : object.h
--*/
#ifndef __OBJECT_H
#define __OBJECT_H

#include <iostream.h>
#include <stddef.h>

#include "cplusplu.hh"


class Object;

typedef void (*ITERATE)( Object& o, void* parameters );
typedef void (*ITERATE_CONST)( const Object& o, void* parameters );
typedef int (*ITERATE_TEST)( const Object& o, void* parameters );

/****************************************************************************
  class Object
--*/

class Object 
{
#if defined( __TURBOC__ ) && ( __TURBOC__ >= 0x300 )
friend ostream& operator <<( ostream&, const Object& );
#else
friend inline ostream& operator <<( ostream&, const Object& );
#endif

public:
    void* operator new(size_t);
    void dprint(); // debug print. callable from within gdb
     
    virtual int operator ==( const Object& ) const;
    int operator !=( const Object& test ) const;
//    virtual void printOn( ostream& os ) const = 0;
    virtual void printOn( ostream& os ) const { os << "an Object"; }// = 0; 

    virtual void each( ITERATE action, void* parameters );
    virtual void each( ITERATE_CONST action , void* parameters ) const;
    virtual Object& first( ITERATE_TEST test, void* parameters );

    Object();
    virtual ~Object();
};

#if 1
#define __OPERATOR_NEW
inline void* Object::operator new( size_t size )
{
    return ::operator new( size );
}
#endif

inline Object::Object()
{
}

inline Object::~Object()
{
}

inline int Object::operator ==( const Object& test ) const
{  
    return ( this == &test );
}

inline int Object::operator !=( const Object& test ) const
{  
    return !( *this == test ); 
}

inline ostream& operator <<( ostream& os, const Object& o )
{
    o.printOn( os );
    return os;
}
//-- class Object //

/****************************************************************************
  class ErrorObject
--*/

class ErrorObject : public Object 
{
public:
    void operator delete( void* );

    virtual void printOn( ostream& os ) const;
    
    ErrorObject();
    virtual ~ErrorObject();
};

extern ErrorObject errorObject;
#define ZERO ( &(Object&)errorObject )
#define NOOBJECT ( *ZERO )

inline ErrorObject::ErrorObject()
{
}

inline void ErrorObject::operator delete( void* )
{
//    cerr << "attempt to delete the null object\n";
}

inline ErrorObject::~ErrorObject()
{
}

inline void ErrorObject::printOn( ostream& os ) const
{
    os << "The error object" << endl;
}
//-- class ErrorObject //

#endif // __OBJECT_H  //
