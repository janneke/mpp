/*
   path.cc - manipulation of paths and filenames.
*/
#include <stdio.h>
#include "globals.hh"
#include "path.hh"

#ifndef PATHSEP
#define PATHSEP '/'
#endif

/// make input file name: add default extension. "" is stdin.
void
destill_inname( String &inName)
{
    if ( inName.len() )
        {
        if( inName[ 0 ] != '-' ) 
	    {
	    String a,b,c,d;
	    split_path(inName,a,b,c,d);

	    // add extension if not present.
	    if (d == "") 
		d = ".mpp";
	    inName = a+b+c+d;
	    }
	} else inName = "";   
}

void
split_path(String path, 
	   String &drive, String &dirs, String &filebase, String &extension)
{
    // peel off components, one by one.
    int di = path.pos(':');
    if (di) 
	{
	drive = path.left(di);
	path = path.right(path.len() - di);
	} 
    else
	drive = "";
    
    di = path.lastPos(PATHSEP);
    if (di) 
	{
	dirs = path.left(di);
	path = path.right(path.len()-di);
	}
    else
	dirs = "";
    
    di = path.lastPos('.');
    if (di) 
	{
	di --; // don't forget '.'
	filebase = path.left(di);
	extension =path.right(path.len()-di);	
	} 
    else 
	{
	extension = "";   
	filebase = path;
	}
}
/**
   INPUT: path the original full filename
   OUTPUT: 4 components of the path. They can be empty
*/

void
File_path::add(String p) {
//    if (p.right(1) != PATHSEP)
//	p += char(PATHSEP);
    
    svec<String>::add(p);
}

File_path::File_path(String pref) {
    add(".");
    String a,b,c,d;
    split_path(pref, a,b,c,d);
    add(a+b);
}


///
String
File_path::find(String nm){
     for (int i=0; i < sz(); i++) {
	 String path  = (*this)[i];
	 path+= "/"+nm;
//	 *mlog << "trying <" << path<<">";

	 // yes. Ugly. i know.
	 FILE *f = fopen(path, "r");
	 if (f) {
	     fclose(f);
	     return path;
	 }
     }
     
     error("cannot find input file \'" + nm + "\'");
     return "";
}
/**
  It will search in the current dir, in the construction-arg, and
  in any other added path, in this order.
  */
