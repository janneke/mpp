#ifndef PATH_HH
#define PATH_HH
#include "string.hh"
#include "vray.hh"


///   searching directory for file.
class File_path : private svec<String>
{
public:
    /// locate a file in the search path
    String find(String nm);

    /// construct using prefix. Normally argv[0].
    File_path(String);

    /// add to end of path.
    void add(String);
};
/**

   Abstraction of PATH variable. An interface for searching input files.
   Search a number of dirs for a file.
   
*/

extern File_path *search_path;

/// split path into its components
void split_path(String path, String &drive, String &dirs, String &filebase, String &extension);

/// make input file name: add default extension. "" is stdin.
void destill_inname( String &inName);
#endif
