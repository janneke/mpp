class String;
class TextRecord;
class ofstream;
class ifstream;
class istream;
class ostream;
class Bar;
class BarIterator;
class Duration;
class InitiateBeam;
class InitiateSlur;
class Interval;
class Key;
class NoteName;
class Chord;
class Object;
class Staff;
class Note;
class SimpleNote;

#define system_error( s )      error( s, __FILE__, __LINE__ )
#define system_warning( s )    warning( s, __FILE__, __LINE__ )
extern void error( String s, String file="", int line =0);
extern void warning( String s, String file="", int line =0);
