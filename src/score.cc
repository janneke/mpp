/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : score.cc
  AUTHOR : JCNieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include "config.hh"
#include <fstream.h>
#include <time.h>
#include <ctype.h>
#include "config.hh"
#ifdef __unix__
#include <strstream.h>
#else
#include <strstrea.h>
#endif
#include <stdlib.h> // abs..
#include "globals.hh"
#include "init.hh"
#include "mpp.hh"
#include "duration.hh"
#include "key.hh"               // execute
#include "clef.hh"              // barToken
#include "bar.hh"
#include "maclist.hh"
#include "score.hh"
#include "version.hh"
#include "path.hh"


/****************************************************************************
  class Score
--*/


Score::Score() :
    codeFile( 0 ),
    StaffList()
{
}


/*
 * jcn: why is this func?
   hwn: because of separation of actions. 1 func = 1 action
 */

void
Score::setOutput( String name )
{
//    if ( codeFile )		// jcn: check added
    // hwn: read the neukende c++ standard.
	delete codeFile;    
    outName = name;
        
    if (outName == "") 
        {
        outName = "<stdout>";
        codeFile = &cout;
	mlog = &cerr;//??
        }
    else
        codeFile = new ofstream( outName );

    if (!*codeFile)
	error( quoteString( "can't create", outName ), 0,0 );
}


Score::~Score()
{
#if 1
//    if ( codeFile )
        delete codeFile;
#endif
}

void
Score::doFooter()
{
    *codeFile << "%%% END BODY\n";

    if ( !( mode & EXTRACT ) )
        {
        *codeFile << "\\endpiece" << endl;
        *codeFile << "\\endmuflex" << endl;
        *codeFile << "\\end" << endl;
        }
    else
        *codeFile << "\\endextract" << endl;
	*codeFile << "%%% ENDFOOTER\n";
}


void
Score::doHeader()
{
    /****************
      IDENTIFY
     ****************/
    *codeFile << "% MusiXTeX Source created by mpp ";
				// let-s only put version nr.
    *codeFile << '%'<< VERSION_STRING << " ";

    time_t now = time( 0 );
    *codeFile << asctime( localtime( &now ) );

    *codeFile << "% File: " << outName << endl;

    *codeFile << "% From: ";
    ScoreIterator staffs( *this );
    while ( staffs )
        {
        Staff& staff = staffs++;
        if ( staff.instrumentStaff && staffs )
            {
            *codeFile << '(';
            *codeFile << (String)staff.inName;
            *codeFile << ", ";
            *codeFile << (String)staffs++.inName;
            *codeFile << ')';
            }
        else
            *codeFile << (String)staff.inName;
//        if ( &staffs() != &NOSTAFF )
        if ( staffs )
            *codeFile << ", ";
        }

    *codeFile << endl;
    *codeFile << '%' << endl;


    // insert mpp.tex
    if ( !( mode & EXTRACT ) )
        {
        String macrosName = search_path->find("mpp.tex");
        ifstream macrosFile( macrosName );

        if ( !macrosFile ) 
	    {
            error( quoteString( "can't open", macrosName));
            error( "Please reinstall mpp properly");
	    }
        char c;
        while ( ( c = macrosFile.get() ) != (char)EOF )
           codeFile->put( c );
        *codeFile << '%' << endl;
        }

    *codeFile << "\\instrumentnumber{"<<
      Staff::instrumentCount << "}%\n"<< endl;
                               // reversed order
    staffs.reset();
    while ( staffs )
        {
        Staff& staff = staffs++;
        Staff& nextStaff = staffs();
	if ( ( &nextStaff != &NOSTAFF ) && ( staff.instrumentNumber == nextStaff.instrumentNumber ) )
            {
            *codeFile << "\\setstaffs" << ( staff.instrumentNumber + 1 );

	    // \setstaffs{1} ipv \setstaffs1?


            *codeFile << '{' << ( staff.instrumentStaff + 1 ) << '}' << endl;
            *codeFile << '%' << endl;
            staffs++;
            }
        else
            {
            *codeFile << "\\setstaffs" << ( staff.instrumentNumber + 1 );
            *codeFile << '{' << ( staff.instrumentStaff + 1 ) << '}' << endl;
            *codeFile << '%' << endl;
            }
        }

#if 0
  \setname1{Piano}
  \setsign1{-3}
  \interstaff{11}
#endif

    *codeFile << "\\def\\thetitle{" << bottom().inName << '}' << endl;
    *codeFile << "%    each( Staff::invokeDoMacros)\n";


    each( Staff::invokeDoMacros, (void*)(ostream*)codeFile );
    *codeFile << '%' << endl;

    if ( !( mode & EXTRACT ) )
        {
	  *codeFile << "\\CommonTitleDefs" <<endl;
        }


    if ( Staff::instrumentCount > 2 )
        {
	  *codeFile << "\\MoreInstrumentDefs" << endl;
        }

    else
        {
	  *codeFile << "\\OneInstrumentDef"<<endl;
        }

    *codeFile << "\\ppff" << endl;
    *codeFile << '%' << endl;

    if ( !( mode & EXTRACT ) )
        *codeFile << "\\startpiece" << endl;
    else
        {
        *codeFile << "\\let\\endpiece\\endextract" << endl;
        *codeFile << "\\startextract" << endl;
        }

    *codeFile << "\\addspace\\afterruleskip" << endl;
    *codeFile << "%\n%%% END PROLOG\n%\n";

}



//declare( ConstIterator, Staff );


// checks duration of each bar (vertically), prints warning
void
Score::checkBarDuration()
{
    ScoreIterator staffs( *this );

    while ( staffs )
        {
        Staff& staff = staffs++;
        int leftOver = staff.duration - Staff::barDuration;
        if ( leftOver )
            {
            char buf[ 81 ];
            ostrstream oss(buf, 80 );
            oss <<
	      "bar " <<  staff.barCount <<
	       ((leftOver > 0) ? " overfull: " : " underfull: ")
	       //<<Duration( abs( leftOver ) ) <<
		<< duration_to_crotchets(abs(leftOver)) << " crotchets "<<
               (leftOver > 0 ? "over" : "missing")<<ends;

            staff.warning( oss.str());
            }
        staff.duration = 0;
        }
}

/*
   shortest note (vertically). Serves indication to each staff if it
   can print a new note
*/
int
Score::getShortest()
{
    int shortest = 2*DURATION_WHOLE; // Punctuated notes can be longer than a whole. MATSB
    ScoreIterator staffs( *this );

    while ( staffs )
	{
        Staff& staff = staffs++;
        int duration = staff.getDuration();
        if (duration != 0)  // Staffs that run out of notes give 0, MATSB
	    {
	    shortest = min( shortest, abs( duration ) );
	    }
        }
    return shortest;
}

/*
   some notes need more space (accidentals) each staff needs to know
   the current spacing (which is the widest (hor.) of all notes which
   are output
 */

int
Score::getSpacing( int lastSpacing )
{
    int largest = 0;
    ScoreIterator staffs( *this );

    while ( staffs )
        {
        Staff& staff = staffs++;
        int spacing = staff.getSpacing( lastSpacing );
        if ( spacing > largest )
            largest = spacing;
        }
    return largest;
}

/*
   non const printOn
*/

void
Score::printBarOn( ostream& os )//  (const)
{
    ;// monitor << "Score::printBarOn" << endl;

    ScoreIterator staffs( *this );
//    genericStaffConstIterator staffs( *this );

    int shortest = 0;
                               // while notes in (first) bar

//     // bottom is just a member of the list (could have been top())
//     while ( bottom().getDuration() )
//  Changed condition, so no notes are deleted, MATSB
    int remainingNotes=1;
    while (remainingNotes)
        {
        remainingNotes=0; // MATSB
        each( Staff::invokePrintMacros, &os );

        ;// monitor << "lastShortest:" << shortest << endl;
        int signSpacing = getSpacing( shortest );

                               // note spacing according to shortest note
        shortest = getShortest();

        // whugh. doubly used.
        Duration durationSpacing( shortest );
        os << durationSpacing.spacing();

        staffs.reset();

        /* stafflist ordered bottom to top... */
        while ( staffs )
              {
              Staff& staff = staffs++;

              staff.printSpacing( os, signSpacing ); // print some whitespace

              // print as many notes as possible in "shortest" duration.
              // (only one can be printed,  actually)
              staff.printDuration( os, shortest ); // and fill to fit available space
              remainingNotes = remainingNotes || staff.remainingNotes();//MATSB
              }

        os << "\\en%" << endl;
        }

    // (global) macros from a score are supposed to be in
    // bottom bar .

    if ( bottom().bar )
        {
        os << bottom().bar->macroList;
//        os << endl << '%';
        if ( bottom().bar->barToken )
            os << *bottom().bar->barToken << '%' << endl;
#if 0
        if ( Staff::changeContext )
            {
            *codeFile << "\\zchangecontext\n";
            Staff::changeContext = 0;
            }
#endif
        }
    else
        os << "\\bar" << endl;

    if ( Staff::barDuration )
        checkBarDuration();
}

/*
  mingling functinos too much.
 */
void
Score::process()
{
    doHeader();			// input header, output header

    bool _eof = false;
                                // progress indicator
    Staff::barCount = 1;

    do
        {

        *mlog << '[' << flush;

	bool readBar = false;


        ScoreIterator staffs( *this );
        while ( staffs )			  // get one  bar from each  file
            {
            Staff& staff = staffs++;
            staff.getBar();
            if ( !staff.bar )
                staff.error( "Score::process: no bar" );
            ;// monitor << "before readBar [" << Staff::barCount << ']' << endl;
            if ( staff.bar->count() )
                readBar = true;
            ;// monitor << "after readBar" << endl;
            }

        *mlog << Staff::barCount << flush;

        if ( readBar )				  // calc & print
            {
            each( Staff::invokeCalculate );
            printBarOn( *codeFile );
            }

        *mlog << "] " << flush;
	Staff::barCount++;

        *codeFile << '%' << endl;

        staffs.reset();
        while ( staffs )
            {
            Staff& staff = staffs++;
            WhiteSpace ws( staff );		  // eat space
            _eof = (_eof || ( ws.length < 0 ));
#if 1
            if ( staff.newKey )
                {
                staff.key = staff.newKey;
                staff.newKey = 0;


                staff.key->execute( *(StringList*)ZERO, staff );
                }
#endif
            }

        if ( Staff::newBarDuration )		  // new meter
            {
            Staff::barDuration = Staff::newBarDuration;
            Staff::newBarDuration = 0;
            }

    } while ( !_eof );

//    if ( staff._eoBar )
//        staff.warning( "unexpected end of file" );

    doFooter();

    *mlog << " -> " << outName << endl;
}


