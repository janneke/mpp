// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : score.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __SCORE_H
#define __SCORE_H

#include "globals.hh"
#include "string.hh"
#include "staflist.hh"

/****************************************************************************
  class Score
--*/

class Score : public StaffList 
{
    void checkBarDuration();
			  /* begin/end of file */
    void doFooter();
    void doHeader();
    int getShortest();
    int getSpacing( int lastSpacing ); /* shortest note (vertically) in staff. This is "bottleneck" for
						   spacing. */
    /*virtual*/ void printBarOn( ostream& os );
    String outName;
    ostream* codeFile; 
public:

    void process(); /* big work done here */
    void setOutput(String name);    
    Score();
    virtual ~Score();
};

#define ScoreIterator StaffListIterator
//-- class Score //

#endif // __SCORE_H //
