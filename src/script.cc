/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : script.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include <ctype.h>

#include "script.hh"
#include "notename.hh"
#include "simpnote.hh"
//#include "note.hh"
#include "init.hh"
#include "staff.hh"
#include "mpp.hh"

/****************************************************************************
  class Script
--*/

const int Script::adjustPitchArray[ 7 ][ 3 ] =
{                              // FEATURE
      0,   0,   0,             // ACCENT
      0,   0,   0,             // CRESC
    - 4,   1,   2,             // FINGER
      0,   0,   0,             // SLUR
      0,   0,   0,             // SPACE
    - 5,   1,   3,             // STAFF
    - 5,   1,   2              // TEXT
};

//int* Script::adjustPitch[] = &adjustPitchArray[ 0 ][ 1 ];

const int Script::adjustStemArray[ 7 ] =
{                              // FEATURE
    Note::stemLength,          // ACCENT
    0,                         // CRESC
    Note::stemLength,          // FINGER
    0,                         // SLUR
    0,                         // SPACE
    Note::stemLength,          // STAFF
    Note::stemLength           // TEXT
};

const int
Script::heightArray[ 7 ] =
{                              // FEATURE
    1,                         // ACCENT
    3,                         // CRESC
    3,                         // FINGER
    1,                         // SLUR
    1,                         // SPACE
    3,                         // STAFF
    3                          // TEXT
};

Script::Script( Feature& feature ) :
    Feature( feature ),
    orientation( UNDEFINED ),
    pitch( 0 ),
    pitchAdjust( 0 )
{
}

Script::Script( Type type, Orientation o ) :
    Feature( NOSIMPLENOTE, type ),
    orientation( o ),
    pitch( 0 ),
    pitchAdjust( 0 )
{
}

Script::Script( Staff& staff, SimpleNote& note, Orientation o ) :
    Feature( note, ACCENT ),
    orientation( o ),
    pitch( 0 ),
    pitchAdjust( 0 )
{
    getFrom( staff );
}

Script::Script( String name, String substitute, Type type, Orientation orientation, int parameterCoun ) :
    Feature( name, substitute, type, parameterCoun ),
    orientation( orientation ),
    pitch( 0 ),
    pitchAdjust( 0 )
{
}


Script::~Script()
{
    ;// monitor << "~Script" << endl;
}

#if 0
void
Script::operator =( Feature& feature )
{
    type = feature.type;
}
#endif

void
Script::calculate( SimpleNote& note )
{
    ;// monitor << "Script::calculate" << endl;

    if ( orientation == UNDEFINED )
        orientation = Orientation( -note.orientation );

//    pitchAdjust = 0;
    pitch = note.featurePitch[ orientation ];

    ;// monitor << " o: " << orientation;
    ;// monitor << " p: " << pitch;
    ;// monitor << " pA: " << pitchAdjust;
    ;// monitor << " t: " << type << endl;

//        pitchAdjust += max( note.staff.midPitch + 3 - ( pitch + pitchAdjust ), 0 );
//        pitchAdjust += min( ( note.staff.midPitch - 3 ) - ( pitch + pitchAdjust ), 0 );
//        pitchAdjust += -max( - note.staff.midPitch + 3 + ( pitch + pitchAdjust ), 0 );

    if ( orientation == Orientation( note.orientation ) )
        pitchAdjust += orientation * adjustStemArray[ type ];

    pitchAdjust += adjustPitchArray[ type ][ UNDEFINED + 1 ] * orientation *
        max( orientation * note.staff.midPitch + 3
        - orientation * ( pitch + pitchAdjust ),
        0 );

    pitchAdjust += adjustPitchArray[ type ][ orientation + 1 ];

    note.featurePitch[ orientation ] += orientation * heightArray[ type ];

    ;// monitor << " o: " << orientation;
    ;// monitor << " p: " << pitch;
    ;// monitor << " pA: " << pitchAdjust;
    ;// monitor << " nfP: " << note.featurePitch[ orientation ] << endl;
}

int
Script::getFrom( Staff& staff )
{
    ;// monitor << "Script::getFrom" << flush;

    istream& is = *staff.is;

    char c = is.peek();

                               // temporarily -, default feature too
//    staff.expect( '_' );
    is.get( c );

    return Feature::getFrom( staff );
}

#if 0
void
Script::execute( StringList& parameterList )
{
    ;// monitor << "Script::getFrom" << flush;

    String s = parameterList.top();

    if ( isdigit( s[ 0 ] ) && ( s[ 1 ] == '\0' ) )
        {
        substitute = String ( "finger{" ) + s + '}';
        type = FINGER;
        }
    else
        Feature::execute( parameterList );
}
#endif

void
Script::printOn( ostream& os ) const
{
    ;// monitor << "Script::printOn" << flush;

    if ( type == STAFF )
        os << "\\atswap";
    else
        os << ( orientation == UP ? "\\u" : "\\l" );

    os << substitute;

    os << '{' << NoteName( pitch + pitchAdjust ) << '}';
}
//-- class Script //
