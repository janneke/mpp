/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : script.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __SCRIPT_H
#define __SCRIPT_H

class istream;
class ostream;
#include "feature.hh"

/****************************************************************************
  class Script
--*/

class Script : public Feature 
{
 friend class SimpleNote;      // adjust pitchAdjust
 
 public:
    Orientation orientation;
    int pitch;

 protected:
    static const int adjustPitchArray[ 7 ][ 3 ];
//    static int* adjustPitch; // ?? how ??
    static const int adjustStemArray[ 7 ];
    static const int heightArray[ 7 ];

    int pitchAdjust;
    
 public:
//    virtual void operator =( Feature& feature );

    virtual void calculate( SimpleNote& note );
    virtual int getFrom( Staff& staff );
    virtual void printOn( ostream& os ) const;

    Script( Feature& feature );
    Script( Type type, Orientation orientation = Token::UNDEFINED );
    Script( Staff& staff, SimpleNote& note, Orientation orientation = Token::UNDEFINED );
    Script( String name, String substitute = "" , Type type = ACCENT, Orientation o = UNDEFINED , int parameterCount = 0 );
    virtual ~Script();
};

#define ZEROSCRIPT ((Script*)ZERO)
#define NOSSCRIPT (*ZEROSCRIPT)
//-- Script //

#endif // __SCRIPT_H //
