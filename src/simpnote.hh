// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : simpnote.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __SIMPLENOTE_H
#define __SIMPLENOTE_H


#include "note.hh"


class Bar;
class Duration;
class Feature;
class InitiateBeam;
class InitiateSlur;
class Macro;
class NoteName;
class Score;
class Staff;
class TerminateBeam;
class TerminateSlur;

#ifndef MacroList
#define MacroList GENERIC( Macro, READABLE( List ) )
#endif
#ifndef FeatureList
#define FeatureList GENERIC( Feature, List )
#endif
#ifndef SlurList
#define SlurList GENERIC( InitiateSlur, READABLE( List ) )
#define TerminateSlurList GENERIC( TerminateSlur, READABLE( List ) )
#endif
#ifndef BeamList 
#define BeamList GENERIC( InitiateBeam, READABLE( List ) )
#define TerminateBeamList GENERIC( TerminateBeam, READABLE( List ) )
#endif

class MacroList;
class FeatureList;
class SlurList;
class TerminateSlurList;
class BeamList;
class TerminateBeamList;

/****************************************************************************
  class SimpleNote
--*/

class SimpleNote : public Note 
{
 friend class Note;            // beamList, macroList, slurList
 friend class Chord;           // stem, orientation
 friend class Script;          // featurePitch, orientation
 friend class InitiateSlur;    // beam, orientation, stem
 friend class StemFeature;     // stem
 friend class StyleFeature;    // style

    void printResetSizeOn( ostream& os ) const;
    void printSetSizeOn( ostream& os ) const;
    void printStyleOn( ostream& os ) const;

public:
    static void invokeCalculateStem( Object& note, void* )
    {
        ( (SimpleNote&)note ).calculateStem();
    }

 protected:
                               // read order:
    BeamList& beamList;        // 1
    FeatureList& crescendoList;// 2
    SlurList& slurList;        // 3
    NoteName& name;            // 4
    Duration& _duration;       // 5
    FeatureList& featureList;  // 6
                               // 7
    TerminateSlurList& terminateSlurList;
                               // 8
    TerminateBeamList& terminateBeamList;

    InitiateBeam& beam;        // the current beam, e.g. of other note of chord

    // should be in beamnote?
    int beamNumber;            // may be set by feature

    int featurePitchArray[ 3 ];			  /* 0 = up, 1 = level of note, 2 = down */
    // pitch should be positioning
    int *const featurePitch;
    /* height of the super/subscripts. To add an accent: featurePitch[sign(accent)]+= sign(accent) */
    // accent itself is stored in featureList
    

    StemOrientation orientation;
    int signShift;
    Stem stem;
    Style style;

    virtual void calculateFeaturePitch();
    virtual void calculateStem();

    virtual void printDurationOn( ostream& os ) const;
    void printListsOn( ostream& os ) const;
    virtual void printNameOn( ostream& os ) const;
    virtual void printSignOn( ostream& os ) const;
    virtual void printStemOn( ostream& os ) const;

public:
    virtual void calculate();
    virtual int duration();
    static SimpleNote& getSimpleNote( Staff& staff );
    virtual int pitch() const;
    virtual void printOn( ostream& os ) const;
    virtual int spacing( int additionalSpacing );

    SimpleNote( const SimpleNote& note );
    SimpleNote( Staff& staff );
    virtual ~SimpleNote();
};

#define ZEROSIMPLENOTE ((SimpleNote*)ZERO)
#define NOSIMPLENOTE   (*ZEROSIMPLENOTE)

//-- SimpleNote //

#endif // __SIMPLENOTE_H //
