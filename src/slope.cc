/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : slope.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/


#include "beam.hh"
#include "duration.hh"
#include "notename.hh"
#include "slope.hh"
#include "init.hh"
#include "staff.hh"
#include "mpp.hh"

/****************************************************************************
  class Slope
--*/

Slope::Slope( String name ) :
    Macro( name, "",  1 )
{
    ;// monitor << "Slope::Slope";
}

Slope::~Slope()
{
    ;// monitor << "~Slope";
}

void
Slope::execute( StringList& parameters, Staff& )
{
    ;// monitor << "Slope::execute" << flush;

    substitute = "";

    int slope = String(parameters.top() ).value();
#if 0
    InitiateBeam::slopeFactor = slope;
#else
    InitiateBeam::noteSkipFactor = slope;
#endif

    ;// monitor << slope << endl;
}
//-- class Slope //


/****************************************************************************
  class StemLength
--*/

StemLength::StemLength( String name ) :
    Macro( name, "",  1 )
{
    ;// monitor << "StemLength::StemLength";
}

StemLength::~StemLength()
{
    ;// monitor << "~StemLength";
}

void
StemLength::execute( StringList& parameters, Staff& )
{
    ;// monitor << "StemLength::execute" << flush;

    substitute = "";
    int stem = String( parameters.top() ).value();
    if ( stem )
        InitiateBeam::minimumStem = stem;
    else
        InitiateBeam::minimumStem = 4; //BEAM_NORMALSTEM;
    ;// monitor << stem << endl;
}
//-- class StemLength //


/****************************************************************************
  class Octavate
--*/
                               // why not merged with class Octave ?

Octavate::Octavate( String name ) :
    Macro( name, "",  1 )
{
    ;// monitor << "Octavate::Octavate";
}

Octavate::~Octavate()
{
    ;// monitor << "~Octavate";
}

void
Octavate::execute( StringList& parameters, Staff& staff )
{
    ;// monitor << "Octavate::execute" << flush;

//    int slope = (parameters.top() ).value();
    Octave octave( char2istream( parameters.top() ) );
    staff.octavate = octave - Octave::central;
}

/****************************************************************************
  class MusicStyle
--*/

MusicStyle::MusicStyle( String name, String substitute, NESTED_IN(Note)Style s ) :
    Macro( name, substitute, 0 ),
    style( s )
{
    ;// monitor << "MusicStyle::MusicStyle";
}

MusicStyle::~MusicStyle()
{
    ;// monitor << "~MusicStyle";
}

void
MusicStyle::execute( StringList&, Staff& staff )
{
    ;// monitor << "MusicStyle::execute" << flush;

    staff.style = style;
}

//-- class MusicStyle //
