// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : slope.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#ifndef __SLOPE_H
#define __SLOPE_H
#include "macro.hh"

#ifndef __NOTE_H               // Style
#include "note.hh"
#endif

/****************************************************************************
  class MusicStyle
--*/

class MusicStyle : public Macro 
{
    NESTED_IN(Note)Style style;

 public:
    void MusicStyle::execute( StringList&, Staff& staff );
    
    MusicStyle( String name, String substitute, NESTED_IN(Note)Style s );
    ~MusicStyle();
};

//-- class MusicStyle //

/****************************************************************************
  class Octavate
--*/

class Octavate : public Macro
{
 public:
    virtual void execute( StringList& parameters, Staff& staff );

    Octavate( String name );
    virtual ~Octavate();
};

//-- class Octavate //

/****************************************************************************
  class Slope
--*/

class Slope : public Macro 
{
 public:
    virtual void execute( StringList& parameters, Staff& staff );

    Slope( String name );
    virtual ~Slope();
};
//-- class Slope //

/****************************************************************************
  class StemLength
--*/

class StemLength : public Macro
{
 public:
    virtual void execute( StringList& parameters, Staff& staff );

    StemLength( String name );
    virtual ~StemLength();
};
//-- class StemLength //

#endif // __SLOPE_H //

