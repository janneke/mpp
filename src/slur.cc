/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : slur.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include <ctype.h>
#include "init.hh"
#include "notename.hh"
//#include "simpnote.hh"
#include "beamnote.hh"
#include "script.hh"
#include "beam.hh"
#include "slur.hh"
#include "staff.hh"
#include "mpp.hh"

/****************************************************************************
  class InitiateSlur
--*/

InitiateSlur& InitiateSlur::getInitiateSlur( Staff& staff )
{
    WhiteSpace ws( staff );
    char c = staff.is->peek();
    if ( c == '(' )
        return *new InitiateSlur( staff );
    else
        return NOSLUR;
}

InitiateSlur::InitiateSlur( Staff& s ) :
    Script( SLUR ),
    beam( *staff.beam ),
    beamNoteCount( 0 ),
    number( staff.number ),
    staff( s )
{
    ;// monitor << "InititateSlur";

    istream& is = *staff.is;
    staff.expect( '(' );
    char c;
    is.get( c );
    c = is.peek();

    staff.slurCount++;

    if ( isdigit( c ) )
        {
        is.get( c );
        number = int( c - '0' );
#if 1
        number--;
        if ( number )
            number = 6 - staff.number * 2 - ( number + 1 - 2 );
#endif
        c = is.peek();
        }
    else
        {
        if ( staff.slurCount > 1 )
//            number = STAFF_MAX - staff.number * 2 - ( staff.slurCount - 2 );
                               // MusixTeX does not accept slur > 6 ??
            number = 6 - staff.number * 2 - ( staff.slurCount - 2 );
        }

    switch( c )
        {
        case '^'  :
            is.get( c );
            orientation = UP;
            break;
        case 'v'  :
            is.get( c );
            orientation = DOWN;
            break;
        default:
            orientation = UNDEFINED;
            if ( beam != NOBEAM )
                orientation = Orientation( -beam.orientation );
        }
                               // this *can* all be calculated,
                               // but it requires distinction between
                               // accent and other super/subscripts
                               // and makes Beam and Slur classes
                               // too different to inherit from Initiators
                               // Terminators...
    c = is.peek();
    while ( ( ( c == '+' ) || ( c == '-' ) )  && ( c != (char)EOF ) )
        {
        if ( c == '+' )
            pitchAdjust++;
        else
            pitchAdjust--;
        is.get( c );
        c = is.peek();
        }

    if ( beam != NOBEAM )
        beamNoteCount = beam.noteCount + 1;

    staff.slur = this;
}

InitiateSlur::~InitiateSlur()
{
    ;// monitor << "~InitiateSlur";
    staff.slur = ZEROSLUR;
}

void
InitiateSlur::calculate( SimpleNote& note )
{
                               // can we do without member beam?
//    if ( ( orientation == UNDEFINED ) && ( beam != NOBEAM ) )
//        orientation = Orientation( -beam.orientation );
    if ( orientation == UNDEFINED )
        orientation = Orientation( -note.orientation );

    int savePitchAdjust = pitchAdjust;

    Script::calculate( note );

//    if ( ( beam != NOBEAM ) && ( beam.orientation == orientation ) )
//        pitch = beam.effectivePitch( beamNoteCount );
//        pitchAdjust = savePitchAdjust;
//        }

    if ( note.orientation == orientation )
        {
        if ( note.stem & Note::BEAM )
            {
//            pitch = note.beam.effectivePitch( beamNoteCount );
            pitch = note.beam.effectivePitch( ( (BeamNote&)note ).beamNoteCount );
            pitchAdjust = savePitchAdjust;
            }
        else                   // if:: bug fix
             if ( orientation * ( pitch - note.pitch() ) > 2 )
                 pitchAdjust = -orientation * Note::stemLength;
        }
}

void
InitiateSlur::printOn( ostream& os ) const
{

    os << "\\i";

    if ( ( beam != NOBEAM ) && ( beam.orientation == orientation ) )
        os << 'b';

    os << "slur";
                               // MusiXTeX:: why here `d'
    os << ( orientation == UP ? 'u' : 'd' );

    os << number;

    os << '{' << NoteName( pitch + pitchAdjust ) << '}';
}
//-- class InitiateSlur //

/****************************************************************************
  class TerminateSlur
--*/

TerminateSlur& TerminateSlur::getTerminateSlur( Staff& staff )
{
    WhiteSpace ws( staff );
    char c = staff.is->peek();
    if ( c == ')' )
        return *new TerminateSlur( staff );
    else
        return NOTERMINATESLUR;
}

TerminateSlur::TerminateSlur( Staff& s ) :
    Script( SLUR ),
    beam( *staff.beam ),
    beamNoteCount( 0 ),
    number( staff.number ),
    staff( s )
{
    ;// monitor << "TerminateSlur";

    istream& is = *staff.is;
    staff.expect( ')' );
    char c;
    is.get( c );

    c = is.peek();
    if ( isdigit( c ) )
        {
        is.get( c );
        number = int( c - '0' );
#if 1
        number--;
        if ( number )
            number = 6 - staff.number * 2 - ( number + 1 - 2 );
#endif
        c = is.peek();
        }
    else
        {
        if ( staff.slurCount > 1 )
//            number = STAFF_MAX - staff.number * 2 - ( staff.slurCount - 2 );
                               // MusixTeX does not accept slur > 6 ??
            number = 6 - staff.number * 2 - ( staff.slurCount - 2 );
        }

    if ( staff.slurCount < 1 )
        staff.error( "unexpected `)\'" );
    else
        staff.slurCount--;

    switch( c )
        {
        case '^'  :
            is.get( c );
            orientation = UP;
            break;
        case 'v'  :
            is.get( c );
            orientation = DOWN;
            break;
        default:
            orientation = UNDEFINED;
            if ( *staff.slur != NOSLUR )
                orientation = staff.slur->orientation;
            break;
        }

    c = is.peek();
    while ( ( ( c == '+' ) || ( c == '-' ) )  && ( c != (char)EOF ) )
        {
        if ( c == '+' )
            pitchAdjust++;
        else
            pitchAdjust--;
        is.get( c );
        c = is.peek();
        }

    if ( beam != NOBEAM )
        beamNoteCount = beam.noteCount;
}

TerminateSlur::~TerminateSlur()
{
    ;// monitor << "~TerminateSlur";
    staff.slur = ZEROSLUR;
}
                               // why not inherit from Slur::
                               // same routine
void
TerminateSlur::calculate( SimpleNote& note )
{
    if ( ( orientation == UNDEFINED ) && ( beam != NOBEAM ) )
        orientation = Orientation( -beam.orientation );

    int savePitchAdjust = pitchAdjust;

    Script::calculate( note );

    if ( ( beam != NOBEAM ) && ( beam.orientation == orientation ) )
        {
        pitch = beam.effectivePitch( beamNoteCount );
        pitchAdjust = savePitchAdjust;
        }
}

void
TerminateSlur::printOn( ostream& os ) const
{
    os << "\\t";

                               // MusiXTeX:: why the `u'
    if ( ( beam != NOBEAM ) && ( beam.orientation == orientation ) )
        os << "ub" ;

    os << "slur";

    os << number;

    os << '{' << NoteName( pitch + pitchAdjust ) << '}';
}
//-- class TerminateSlur //
