// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : slur.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __SLUR_H 
#define __SLUR_H
#include "script.hh"

class istream;
class ostream;
class InitiateBeam;
class Staff;

/****************************************************************************
  class InitiateSlur
--*/

class InitiateSlur : public Script 
{
protected:
    Staff& staff;
    InitiateBeam& beam;
    int beamNoteCount;
    int number;

public:
#if 0
    static void setPitch( Object& slur, void* pitch )
    {
        ( (InitiateSlur&)slur ).pitch = *(int*)pitch;
    }
#endif

    static InitiateSlur& getInitiateSlur( Staff& staff );
    
    virtual void calculate( SimpleNote& note );
    virtual void printOn( ostream& os ) const;

    InitiateSlur( Staff& s );
    ~InitiateSlur();
};

#define ZEROSLUR ((InitiateSlur*)ZERO)
#define NOSLUR (*ZEROSLUR)
//-- class InitiateSlur //


/****************************************************************************
  class TerminateSlur
--*/

class TerminateSlur : public Script
{
protected:
    Staff& staff;
    InitiateBeam& beam;
    int beamNoteCount;
    int number;
    
public:
#if 0
    static void setPitch( Object& slur, void* pitch )
    {
        ( (TerminateSlur&)slur ).pitch = *(int*)pitch;
    }
#endif    

    static TerminateSlur& getTerminateSlur( Staff& staff );

    virtual void calculate( SimpleNote& note );
    virtual void printOn( ostream& os ) const;

    TerminateSlur( Staff& s );
    ~TerminateSlur();
};

#define ZEROTERMINATESLUR ((TerminateSlur*)ZERO)
#define NOTERMINATESLUR (*ZEROTERMINATESLUR)
//-- class TerminateSlur //

#endif // __SLUR_H //
