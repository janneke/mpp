/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : slurlist.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include "slurlist.hh"

/****************************************************************************
  class SlurList
--*/

implement( ReadableList, InitiateSlur );

//-- class SlurList //

/****************************************************************************
  class TerminateSlurList
--*/

implement( ReadableList, TerminateSlur );

//-- class TerminateSlurList //
