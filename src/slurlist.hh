#ifndef __SLURLIST_H
#define __SLURLIST_H
#include "grlist.hh"
#include "slur.hh"

declare( List, InitiateSlur );
declare( ReadableList, InitiateSlur );

declare( List, TerminateSlur );
declare( ReadableList, TerminateSlur );

#ifndef SlurList
#define SlurList GENERIC( InitiateSlur, READABLE ( List ) )
#define TerminateSlurList GENERIC( TerminateSlur , READABLE ( List ) )
#endif

#endif //__SLURLIST_H //
