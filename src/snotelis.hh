#ifndef __SIMPLENOTELIST_H
#define __SIMPLENOTELIST_H
#include "glist.hh"
#include "simpnote.hh"

declare( List, SimpleNote );
#ifndef NoteList
#define NoteList GENERIC( SimpleNote, List )
#endif
#define NoteListIterator GENERIC( SimpleNote, ListIterator )

#endif //__SIMPLENOTELIST_H //
