// sortable.h -- object which has comparison operations.

#ifndef __SORTABLE_H
#define __SORTABLE_H


#include "object.hh"


/****************************************************************************
  class Sortable
--*/

class Sortable : public Object {
public:
    virtual int operator==(const Sortable& test ) const = 0;
    virtual int operator > (const Sortable& test ) const = 0;
    int operator < ( const Sortable& test ) const;
    int operator <=( const Sortable& test ) const;
    int operator >=( const Sortable& test ) const;
    virtual void printOn( ostream& os ) const = 0;
};

inline int Sortable::operator <( const Sortable& test ) const
{ 
//return !( (*this > test) || (*this == test) ); 
                               //must create both, bitwise or ?
    return !( ( *this > test ) | ( *this == test ) ); 
}

inline int Sortable::operator >=( const Sortable& test ) const
{ 
    return !( *this < test );
}

inline int Sortable::operator <=( const Sortable& test ) const
{ 
    return !( *this > test );
}

//-- class Sortable //

#endif // __SORTABLE_H  //
