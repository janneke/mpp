/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : staff.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include <fstream.h>
#include "init.hh"
#include "key.hh"
#include "bar.hh"
#include "interval.hh"
#include "beam.hh"
#include "slur.hh"
#include "note.hh"
#include "duration.hh"
#include "imacro.hh"
#include "staff.hh"
#include "mpp.hh"



/****************************************************************************
  class Staff
--*/

int Staff::barCount = 0;
//int Staff::beamCount = - 1;
int Staff::barDuration = 0;
int Staff::changeContext = 0;
int Staff::newBarDuration = 0;
int Staff::newLine = 0;
int Staff::instrumentCount = 0;
//int Staff::slurCount = - 1;
int Staff::count = 0;

// STAFF_MAX should be initted dynamically
Staff* Staff::staffs[ STAFF_MAX ] = {
    ZEROSTAFF, ZEROSTAFF, ZEROSTAFF, ZEROSTAFF,
    ZEROSTAFF, ZEROSTAFF, ZEROSTAFF, ZEROSTAFF,
    ZEROSTAFF
};

/* Wow! talk about construction.. */
Staff::Staff( String name, int i ) : // filename, count of staff
                               // should these be list ?
    bar( 0 ),
    beam( ZEROBEAM ),
    beamCount( 0 ),
    duration( 0 ),
    inName( name ),
    instrumentStaff( i ),
    is( 0 ),
    key( 0 ),
    lastDuration( 0 ),
    leftOver( 0 ),             // reset each bar ?
    line( 0 ),
                               // pitch of b, assume treble key
    midPitch( ( 'b' - 'c' + 7 ) % 7 + 4 * 7 ),
    newKey( 0 ),
    notes( 0 ),
    number( count++ ),
    octavate( 0 ),
                               // should these be list ?
    slur( ZEROSLUR ),
    slurCount( 0 ),
    sourceFile( 0 ),
    style( 0 ),
    transpose( 0 ),
    transposedFrom( 0 ),
    xDuration( 4 )
{
#if 0
    if ( ( number >= 0 ) && ( number < STAFF_MAX ) &&
        ( Staff::staffs[ number ] == ZEROSTAFF ) )
	Staff::staffs[ number ] = this;
    else
        warning( "something's wrong with Staff::staffs" ); // error() ?
#endif

    assert( number >= 0  && number < STAFF_MAX  && Staff::staffs[ number ] == ZEROSTAFF ) ;

    // ugh.
    Staff::staffs[ number ] = this;

    
    if ( !instrumentStaff )
        instrumentNumber = ++instrumentCount - 1;
    else
        instrumentNumber = instrumentCount - 1;

    if ( !inName.len() || ( inName[ 0 ] == '-' ) )
        {
        inName = "stdin";
        sourceFile = &cin;
        }
    else
        sourceFile = new ifstream( (const char*)inName );

    if ( !*sourceFile )
        ::error( quoteString( "can't open", inName ));

    *mlog << inName << ':' << endl;

    sourceFile->flags( sourceFile->flags() & ~ios::skipws );

    is = sourceFile;

//    if ( (key = &(Key&)macroList.firstMacro( Token::compare, "C" ) ) == ZERO )
    String string_of_c("C");
    
    if ( (key = &(Key&)macroList.firstMacro( Token::compare, 
					     (void*) &string_of_c) ) == ZERO )
        error( quoteString( "key not found", "C" ) );
    key->execute( (StringList&)NOOBJECT, *this );
}

Staff::~Staff()
{
    delete sourceFile; // whynot
    delete bar;
    delete notes;
}

void
Staff::calculate()
{
    ;// monitor << "Staff::calculate" << endl;

    if ( bar )
        bar->calculate();
}
void
Staff::error( String s ) const
{
    ::error( s, inName, line );
}


void
Staff::warning( String s ) const
{
    ::warning(s, inName,line );
}

void
Staff::expect( char c )
{
    if ( c != is->peek() )
        error( String("`")+ String( c ) + "' expected" );
}


/*
   disposes of old bar, reads a new bar
 */

int
Staff::getBar()
{
    if ( !line )
        line = 1;

    delete bar;
    bar = 0;
    if (notes && (int)(*notes))  // Warn about disgarded notes, MATSB
	{
	warning("Bar in other staff is shorter, notes ignored");
	}
    delete notes;
    notes = 0;
        

    noteCount = 0;
    

    bar = new Bar( *this );                       // INPUT
    notes = new BarIterator( *bar );

    lastDuration = 0;

    return 0;
}

void
Staff::doMacros( ostream& os )
{
    if ( !line )
        line = 1;

    MacroList macroList( *this );		  // INPUT
    os << macroList;
}

int
Staff::getDuration()
{
    ;// monitor << "Staff::getDuration" << endl;

    if ( leftOver < 0 ) // 	< 0, > 0 ?
        return leftOver;

    // current beat has progressed far enough,
    // we could output a new note
    Note& note = (*notes)();
    if ( note != NOOBJECT )
        return note.duration();
    else
        return 0;				  // superfluous?
}

//this is longhand for Duration( 8 ).duration()
static Duration duration8( 8 );
static int spacing8 = duration8.duration();


/*
   space needed for current note
 */
int
Staff::getSpacing( int lastSpacing )
{
    ;// monitor << "Staff::getSpacing: " << number << endl;
    ;// monitor << lastSpacing << "," << lastDuration;
    ;// monitor << ":" << ( ( lastSpacing == lastDuration ) && ( lastSpacing < spacing8 ) );

    Note& note = (*notes)();
    if ( note != NOOBJECT )
        return note.spacing( ( ( lastSpacing == lastDuration ) &&
            ( lastSpacing < spacing8 ) ) );
    else
        return 0;
}

/*
   INPUT: an interval of notes
   RETURN: width (hor) of the notes in the supplied interval.

   needed for spacing beams correctly
 */
int
Staff::noteCount2NoteSkip( const Interval& note ) const // note is not a Note!
{
    int noteSkip = 0;

    Interval& duration = bar->noteCount2Duration( note );

    for ( int i = 0; i < count; i++ )
        noteSkip = max( noteSkip,
            ( staffs[ i ] )->bar->duration2NoteSkip( duration ) );

    delete &duration;

    return noteSkip;
}


/*
   INPUT
   os: output stream
   d: duration.

   try to print notes fitting in a duration d on os

   Staff::printDuration must be changed so the duration counters don't 
   run away just because a bar in another staff was longer.
   leftOver could still be !=0 at the end of a bar, for example to handle
   the input "| c2 e2. | f4 e4 d4 | c1|" without getting accumulated errors.
   All this will hopefully make it easier for the user to see what was wrong 
   in the mpp files and understand how mpp makes the spacing.
   */

void
Staff::printDuration( ostream& os, const int d )
{
    ;// monitor << "Staff::printDuration" << endl;

    // leftover: how much the staff can print
    // d: how much the score allows staff to print
    int duration = d + leftOver;
    Staff::duration += duration;                  // getting further in measure

    leftOver = 0;                                 // superfluous

    if ( duration < 0 )
        os << "\\sk";

    while ( ( duration > 0 ) && (int)(*notes) )   // still notes, still room for more notes
        {
        Note& note = (*notes)++;

        lastDuration = note.duration();           // lastdur. is member
        duration -= lastDuration;

        os << note << '%' << endl;
        }

//    leftOver = duration;
    leftOver = min(0,duration); // Handle the case of missing notes. MATSB

                               //moved because reversed order
    if ( instrumentStaff )     // next staff of this instrument
        os << '|';
    else if ( instrumentNumber )
        os << '&';

//    Staff::duration -= leftOver;
    Staff::duration -= duration; // Since leftover was redefined above, MATSB
}

void
Staff::printMacros( ostream& os )
{
    ;// monitor << "Staff::printMacros" << endl;

    Note& note = (*notes)();
    if ( note != NOOBJECT )
        os << note.macroList;
}

// superfluous
void
Staff::printOn( ostream& os ) const
{
    os << "Staff";
}

void
Staff::printSpacing( ostream& os, const int spacing )
{
    for ( int i = 0; i < spacing; i++ )
        os << "\\smallsk";			  // smallest space we can use.
}

/* 
   Check if all notes have been printed, MATSB
 */

int
Staff::remainingNotes() const
{
    if (notes) 
	{
	return ((int)(*notes));
	}
    else
	return 0;
}

//-- class Staff //
