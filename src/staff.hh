// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : staff.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __STAFF_H
#define __STAFF_H

#include "globals.hh"
#include "string.hh"


/****************************************************************************
  class Staff
--*/

class Staff : public Object {
public:
    static int barCount;			  /* for output (input) indicator */
//    static int beamCount;
    static int barDuration;
    static int changeContext;			  /* musixtex gory detail */
    static int instrumentCount;
    static int newBarDuration;
    static int newLine;
//    static int slurCount;
    static Staff* staffs[ STAFF_MAX ];		  /* -> pointer to parent */
    static int count;

    // following is about the current bar in *this bar
    // notes in bar/staff can access this info
    Bar* bar;
    InitiateBeam* beam;        // should these be list ?
    int beamCount;
    int duration;
    String inName;
    int instrumentNumber;
    int instrumentStaff;
    istream* is;
    Key* key;
    int lastDuration;
    int leftOver;				  /* < 0, should change this. How much printed too much   */
    int line;
    int midPitch;				  /* pitch of middle line of staff, will change after clef/transpose change */
    Key* newKey;
    int noteCount;
    BarIterator* notes;
    int number;
    int octavate;
    InitiateSlur* slur;
    int slurCount;
    istream* sourceFile; 
    int style;                 // we don't want to include note.h 
    int transpose;
    Key* transposedFrom;
    int xDuration;

    static void invokeCalculate( Object& staff, void* )
    {
        ( (Staff&)staff ).calculate();
    }
    static void invokeDoMacros( Object& staff, void* os )
    {
        ( (Staff&)staff ).doMacros( *(ostream*)os );
    }
    static void invokePrintMacros( Object& staff, void* os )
    {
        ( (Staff&)staff ).printMacros( *(ostream*)os );
    }
    virtual void calculate();
    int getBar();
    int getDuration();
    int getSpacing( int lastSpacing );
    void error( String s ) const;
    void expect( char c );
    void doMacros( ostream& os );
    int noteCount2NoteSkip( const Interval& note ) const;
    void printDuration( ostream& os, const int d );
    void printMacros( ostream& os );
    virtual void printOn( ostream& os ) const;
    void printSpacing( ostream& os, const int spacing );
    int remainingNotes() const; // Added method, MATSB
    void warning( String s ) const;

    Staff( String name, int i );
    virtual ~Staff();
};

//-- class Staff //

#define ZEROSTAFF ((Staff*)ZERO)
#define NOSTAFF (*ZEROSTAFF)

#endif // __STAFF_H
