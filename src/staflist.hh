#ifndef __STAFFLIST_H
#define __STAFFLIST_H
#include "glist.hh"
#include "staff.hh"

declare( List, Staff );
#define StaffList GENERIC( Staff, List )
#define StaffListIterator GENERIC( Staff, ListIterator )

#endif //__STAFFLIST_H //
