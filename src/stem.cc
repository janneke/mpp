/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : stem.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include <ctype.h>
#include "init.hh"
#include "simpnote.hh"
#include "duration.hh"
#include "staff.hh"
#include "feature.hh"
#include "stem.hh"
#include "mpp.hh"

/****************************************************************************
  class StemFeature
--*/

StemFeature::StemFeature( String name, NESTED_IN( Note )Stem s, int parameterCount ) :
    Feature( name , "", FEATURE, parameterCount ),
    beamNumber( -1 ),
    orientation( Note::UNDEFINED ),
    stem( s )
{
    ;// monitor << "Stem::Stem" << endl;
}

StemFeature::StemFeature( String name, NESTED_IN( Note )Stem s, NESTED_IN( Note )StemOrientation o, int parameterCount ) :
    Feature( name , "", FEATURE, parameterCount ),
    beamNumber( -1 ),
    orientation( o ),
    stem( s )
{
    ;// monitor << "Stem::Stem" << endl;
}

StemFeature::StemFeature( Staff& staff, SimpleNote& note ) :
    Feature( staff, note ),
    beamNumber( -1 ),
    orientation( Note::UNDEFINED ),
    stem( Note::NO )
{
    ;// monitor << "StemFeature::StemFeature" << endl;
}

StemFeature::~StemFeature()
{
    ;// monitor << "~StemFeature" << endl;
}

void
StemFeature::execute( StringList& parameters, Staff& staff )
{
    ;// monitor << "StemFeature::Macro::execute" << endl;

    if ( stem == Note::DEFAULT )
        {
        if ( !parameters.count() )
            staff.error( "stem parameter expected" );
        else if ( parameters.top() == "\\no" )
            stem = Note::NO;
        else
            staff.error( quoteString( "invalid stem", parameters.top() ) );
        }

    if ( orientation == Note::UNDEFINED )
        {
        if ( !parameters.count() )
            staff.error( "stem parameter expected" );
        else if ( parameters.top() == "\\up" )
            orientation = Note::UP;
        else if ( parameters.top() == "\\down" )
            orientation = Note::DOWN;
        else
            staff.error( quoteString( "invalid orientation", parameters.top() ) );
        }

    if ( stem == Note::BEAM )
        {
        if ( !parameters.count() )
            staff.error( "beam number expected" );
        int n = (parameters.top() ).value();
        if ( ( beamNumber < 0 ) || ( beamNumber > 9 ) )
            staff.warning( quoteString( "invalid beam number", parameters.top() ) );
        else
            beamNumber = n;
        }
}

void
StemFeature::execute( SimpleNote& note )
{
    ;// monitor << "StemFeature::Feature::execute" << endl;

    note.stem = stem;
    if ( orientation )
        note.orientation = orientation;
    if ( beamNumber > -1 )
        note.beamNumber = beamNumber;
}

void
StemFeature::printOn( ostream& ) const
{
}
//-- class StemFeature //

/****************************************************************************
  class StyleFeature
--*/

StyleFeature::StyleFeature( String name, NESTED_IN( Note )Style s ) :
    Feature( name ),
    style( s )
{
    ;// monitor << "Style::Style" << endl;
}

StyleFeature::StyleFeature( Staff& staff, SimpleNote& note ) :
    Feature( staff, note ),
    style( Note::NORMAL )
{
    ;// monitor << "StyleFeature::StyleFeature" << endl;
}

StyleFeature::~StyleFeature()
{
    ;// monitor << "~StyleFeature" << endl;
}

void
StyleFeature::execute( SimpleNote& note )
{
    ;// monitor << "StyleFeature::Feature::execute" << endl;

    note.style = style;
    if ( style == Note::GRACE )
        {
//        if ( note.duration )
//            delete note.duration;
//        note.duration = new Duration( 8 );
        ;
        }
}

void
StyleFeature::printOn( ostream& ) const
{
}
//-- class StyleFeature //

