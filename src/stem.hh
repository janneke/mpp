// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : stem.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __STEM_H
#define __STEM_H

class istream;
class ostream;
#include "feature.hh"

#ifndef __NOTE_H               // Stem
#include "note.hh"
#endif

class SimpleNote;

/****************************************************************************
  class StemFeature
--*/

class StemFeature : public Feature {

protected:
    int beamNumber;
    NESTED_IN( Note )StemOrientation orientation;
    NESTED_IN( Note)Stem stem;

public:
    virtual void execute( SimpleNote& note );
    virtual void execute( StringList& parameters, Staff& staff );
    virtual void printOn( ostream& os ) const;

    StemFeature( String name, NESTED_IN( Note)Stem s, int parameterCount = 0 );
    StemFeature( String name, NESTED_IN( Note )Stem s, NESTED_IN( Note )StemOrientation o, int parameterCount = 0 );
    StemFeature( Staff& staff, SimpleNote& note );
    virtual ~StemFeature();
};
//-- class StemFeature //

/****************************************************************************
  class StyleFeature
--*/

class StyleFeature : public Feature {
    NESTED_IN( Note )Style style;
    
public:
    virtual void execute( SimpleNote& note );
    virtual void printOn( ostream& os ) const;

    StyleFeature( String name, NESTED_IN( Note )Style s );
    StyleFeature( Staff& staff, SimpleNote& note );
    virtual ~StyleFeature();
};
//-- class StyleFeature //

#endif // __STEM_H //
