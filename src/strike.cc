/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : strike.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include "beam.hh"
#include "strike.hh"
#include "note.hh"
#include "notename.hh"
//#include "init.hh"
//#include "mpp.hh"

/****************************************************************************
  class StrikeBeam
--*/

const int StrikeBeam::slope = 4;
#define OFFSET ".75"

StrikeBeam::StrikeBeam( int n, int p, Orientation o, int m ) :
    Initiator( "StrikeBeam" ),
    multiplicity( m ),
    number( n ),
    orientation( ( o ? o : UP ) )
{
    pitch = p - orientation;
}

StrikeBeam::~StrikeBeam()
{
}

void
StrikeBeam::printOn( ostream& os ) const
{
    os << "\\loffset{" OFFSET "}";
    os << "{\\ib";
    os << ( orientation == UP ? 'u' : 'l' );
    os << number;
    os << '{' << NoteName( pitch ) << '}';
    os << slope;
    os << '}';

    if ( multiplicity > 1 )
        {
        os << "\\loffset{" OFFSET "}";
        os << "{\\n";
        os << String( 'b', multiplicity );
        os << ( orientation == UP ? 'u' : 'l' );
        os << number;
        os << '}';

        for ( int i = multiplicity; i > 1; i-- )
            {
            os << "\\roffset{" OFFSET "}";
            os << "{\\t";
            os << String( 'b', i );
            os << ( orientation == UP ? 'u' : 'l' );
            os << number;
            os << '}';
            }
       }

    os << "\\roffset{" OFFSET "}";
    os << "{\\tb";
    os << ( orientation == UP ? 'u' : 'l' );
    os << number;
    os << '}';
}
//-- class StrikeBeam //

/****************************************************************************
  class StrikeInitiateBeam
--*/

StrikeInitiateBeam::StrikeInitiateBeam( InitiateBeam& m, int mult ) :
    Initiator( "StrikeInitiateBeam" ),
    mother( m ),
    multiplicity( mother.multiplicity + mult )
{
}

StrikeInitiateBeam::~StrikeInitiateBeam()
{
}

void
StrikeInitiateBeam::printOn( ostream& os ) const
{
    os << "\\loffset{" OFFSET "}";
    os << "{\\n";
    os << String( 'b', multiplicity );
    os << ( mother.orientation == UP ? 'u' : 'l' );
    os << mother.number;
    os << '}';

    for ( int i = multiplicity; i > 1; i-- )
        {
        os << "\\roffset{" OFFSET "}";
        os << "{\\t";
        os << String( 'b', i );
        os << ( mother.orientation == UP ? 'u' : 'l' );
        os << mother.number;
        os << '}';
        }
}
//-- class StrikeInitiateBeam //
