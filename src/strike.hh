// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : strike.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __STRIKE_H 
#define __STRIKE_H
#include "macro.hh"

class istream;
class ostream;
class Staff;
class InitiateBeam;

/****************************************************************************
  class StrikeBeam
--*/

class StrikeBeam : public Initiator
{
    static const int slope;
    
    int multiplicity;
    int number;
    Orientation orientation;
    int pitch;

    virtual void printOn( ostream& os ) const;

 public:
    StrikeBeam( int n, int p, Orientation o, int m );
    ~StrikeBeam();
};
//-- class StrikeBeam //

/****************************************************************************
  class StrikeInitiateBeam
--*/

class StrikeInitiateBeam : public Initiator
{
    InitiateBeam& mother;
    int multiplicity;
    
    virtual void StrikeInitiateBeam::printOn( ostream& os ) const;

 public:    
    StrikeInitiateBeam( InitiateBeam& m, int mult );
    ~StrikeInitiateBeam();
};
//-- class StrikeInitiateBeam //

#endif // __STRIKE_H //
