/*
 * PROJECT: flinb C++ library
 * FILE   : string.h
 *
 * Rehacked by HWN 3/nov/95
 * removed String & 's
 * introduced Class String_handle
 */

#ifndef __STRING_HH
#define __STRING_HH
#include <assert.h>
#include <string.h>
class ostream;

#define String mppString // YUK!
//#include "globals.hh"
// should be included from globals.hh

#ifdef CENTRAL_OBJECT
#include "sortable.hh"
#endif

class String_handle;
/// Internal String struct
class StringData {
    // GNU malloc: storage overhead is 8 bytes anyway.
//    const int INITIALMAX = 8;  
      #define INITIALMAX 8

friend class String_handle;
    int maxlen;	// maxlen is arraysize-1
    
    int length;
    char* string;
    int references;

    /// init to ""
    StringData() {
	references=0;
	maxlen = INITIALMAX;
	string = new char[maxlen + 1];
	string[0] = 0;
	length = 0;
    }

    /// init from src. Conservative allocation.
    StringData(StringData const &src) {
	references=0;	
	maxlen = length = src.length;		
	string = new char[maxlen+1]; // should calc GNU 8byte overhead. 	
	strcpy(string, src.string);	
    }
    
    ~StringData() {
	assert(references == 0);
	delete[] string;
    }


    void setmax(int j) {	
	OKW();
	if (j > maxlen) {
	    delete string;
	    maxlen = j;
	    string = new char[maxlen + 1];
	
	    string[0] = 0;
	    length = 0;
	}
    }
    /*@Doc: POST: maxlen >= j.
      IN: j, maximum stringlength.    
      contents thrown away.
    */
    ///
    void remax(int j) {
	OKW();
	if (j > maxlen) {
	    maxlen = j;
	    char *p = new char[maxlen + 1];	    
	    strcpy(p,string);	    
	    delete[] string;
	    string = p;
	//    length = strlen(string);
	}
    }
    /*@Doc POST: maxlen >= j.
      IN: j, maximum stringlength.
      contents are kept if it grows.
      */    
    /// check if writeable.
    void OKW() { assert (references == 1); }

    /// check state.
    void OK() {
	assert(strlen(string) == size_t(length));
	assert(maxlen >= length);
	assert(bool(string));
	assert(references >= 1);
    }

    // needed?
    void update() {
	length  = strlen (string);
    }

    /// reduce memory usage.
    void tighten() { // should be dec'd const
	maxlen = length;
	char *p = new char[maxlen + 1];	    
	strcpy(p,string);	    
	delete[] string;
	string = p;		
    }

    // assignment.
    void set(const char *s) {
	OKW();
	assert(s);
	length = strlen (s);
	remax(length);
	strcpy(string,s);
    }
    
    // concatenation.
    void operator += (const char *s) {
	OK();
	OKW();
	int old = length;
	
	length += strlen(s);
	remax (length);
	strcpy(string + old, s);	
    }

    /// the array itself
    operator const char *() const { return string; }

    // idem, non const
    char *array_for_modify() {
	OKW();
	return string;
    }
    void trunc(int j) {
	OKW(); 
	assert(j >= 0 && j <= length);
	string[j] = 0;
	length = j;
    }

    /** not really safe. Can alter length without StringData knowing it.
      */
    char &operator [](int j) { 
	assert(j >= 0 && j <= length);	
	return string[j] ; 
    }

    char operator [](int j) const { 
	assert(j >= 0 && j <= length);	
	return string[j]; 
    }
};

/**
   the data itself. Handles simple tasks (resizing, resetting)
 */
/// ref. counting for strings
class String_handle {
    StringData* data;
    
    // decrease ref count. Named kind of like a Tanenbaum semafore 
    void down() { if (!(--data->references)) delete data; data = 0; }
    void up(StringData *d) { data=d; data->references ++; }
    
    /** make sure data has only one reference.      
       POST: data->references == 1
      */
    void copy() {
	if (data->references !=1){
	    StringData *newdata = new StringData(*data);
	    down();
	    up(newdata);
	}
    }
    
public:

#if 0
    // Resize the string. Call copy() if necessary
    // do not change contents if  it grows
    // contents thrown away if it shrinks.
    resize(int newlen) {
	copy();
	data->remax(newlen);
    }
#endif

    String_handle() {
	up(new StringData);
    }
    ~String_handle() {	
	down();
    }    
    String_handle(String_handle const & src) {	
	up(src.data);
    }

    // retrieve the actual array.
    operator const char *() const { return *data; }    
    char *array_for_modify() {
	copy();
	return data->array_for_modify();
    }
    
    void operator =(String_handle const &src) {
	if (this == &src)
	    return;
	down();
	up(src.data);
    }
    
    void operator += (const char *s) {	
	copy();
	*data += s;
    }    

    
    char operator[](int j) const { return (*data)[j]; }

    // !NOT SAFE!
    // don't use this for loops. Use array_for_modify()
    char &operator[](int j) {
	copy(); 	// hmm. Not efficient
	return data->array_for_modify()[j];
    }

    void operator = (char const *p) {
	copy();
	data->set(p);
    }
			       
    void trunc(int j) { copy(); data->trunc(j); }
    int len() const { return data->length; }
};
/**
   handles ref. counting, and provides a very thin
   interface using char *
 */


/****************************************************************************
  class String
 */

// whugh
#ifdef CENTRAL_OBJECT
#define _the_stringclass  String : public Sortable
#else
#define _the_stringclass  String
#endif

/// the smart string class.
class _the_stringclass 
{
protected:
    String_handle data; // should derive String from String_handle?

public:

#ifdef CENTRAL_OBJECT // everything derived from Sortable object
    virtual int operator ==( const Sortable& test ) const;
    virtual int operator &&( const Object& test ) const;
    virtual int operator >( const Sortable& test ) const;
#endif


    /// init to ""
    String() {  }                  
    /** needed because other constructors are provided.*/

    /// String s = "abc";
    String( const char* source ); 

    /// "ccccc"
    String( char c, int n = 1 );

    /// String s( 10 );
    String( int i );

    /// 'true' or 'false'
//    String( bool );

    /// String s( 3.14, 6, '#' );
    String ( double f );
    String(  int i,  int n,  char c = ' ' );

    ///  return a "new"-ed copy of contents
    char *copy_array() const;
    virtual operator const char *() const; // virtual???
    
    String operator =( const String & source ) { data = source.data; return *this; }
    void operator += (const char *s) { data += s;}
    void operator += (String s);

    char operator []( int n ) const { return data[n]; }
    
       /// return n leftmost chars
    String left( int n ) const;    
     /// return n rightmost chars
    String right( int n ) const;

     /// convert this to upcase
    String upper();
     /// convert this to downcase 
    String lower();
    String reversed() const;

    String mid(int pos,  int n ) const;
    String nomid(int pos, int n ) const;

    /// signed comparison,  analogous to strcmp;
    int  compare( const char* s ) const;

     /// index of rightmost c     
    int lastPos( char c) const;
    int lastPos( const char* string ) const;

     /// index of leftmost c
    int pos(char c ) const; 
    int pos(const char* string ) const;
    int posAny(const char* string ) const;
    
    void printOn(ostream& os) const;
    long value() const;
    int len() const;
};
/** 

  Intuitive string class. provides 

  ref counting thru #String_handle#

  conversion from bool, int, double, char *, char.

  conversion to int, upcase, downcase


  printable. 

  indexing (pos, posAny, lastPos)

  cutting (left, right, mid)

  concat (+=, +)

  signed comparison (<, >, ==, etc)
*/

// because const char* also has an operator ==, this is for safety:
inline bool operator==(String s1, String s2){    return !(s1.compare(s2));}
inline bool operator==(String s1, const char *s2){ return !(s1.compare(s2));}
inline bool operator==(const char *s1, String s2){ return !(s2.compare(s1));}
inline bool operator!=(String s1, const char *s2  ) {    return s1.compare(s2);}
inline bool operator!=(const char *s1,String s2) {    return s2.compare(s1);}
inline bool operator!=(String s1, String s2  ) {    return s1.compare(s2);}

inline String
operator  + (String s1, String  s2)
{
    s1 += s2;
    return s1;
}

inline ostream &
operator << ( ostream& os, String d )
{
    d.printOn(os);
    return os;
}


/****************************************************************/
String quoteString(String message, String quote);

#endif // __STRiNG_H //
