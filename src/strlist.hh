#ifndef __STRINGLIST_H
#define __STRINGLIST_H

#include "glist.hh"
#include "string.hh"

declare( List, String );
#define StringList GENERIC( String, List )
#define StringListIterator GENERIC( String, ListIterator )

#endif //__STRINGLIST_H //
