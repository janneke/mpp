#include <stdio.h>
#include <ctype.h>

#include <stdlib.h> // exit, barf use error.cc:error/message
#include "globals.hh"
#include "string.hh"
#include "vray.hh"

/*

 a stream for textfiles. linecounting. Thin interface getchar and
 ungetchar.  (ungetc is unlimited) 

 should protect get and unget against improper use
*/
class text_stream
{
    int line_no;
    FILE *f;     // problems with streams on linuctic.
    sstack<char> pushback;
    String name;
    
 public:
    text_stream(String fn)  {
	
	if (fn == "") 
	    {
	    name = "<STDIN>";	    
	    f = stdin;
	    }
	
	else 
	    {
	    name = fn;	    
	    f = fopen(fn, "r");
	    }
	
	if (!f)
	    error("can't open");

	line_no = 1;
    }
    bool eof() {
	return feof(f);
    }
    bool eol() {
	return (peek() == '\n');
    }
    char peek() {
	char c = get();
	unget(c);
	return c;
    }
    int line(){
	return line_no;
    }

    char
    get() {
	char c;
	
	if (pushback.empty())
	    c = getc(f);	
	else 
	    c = pushback.pop();

	if (c =='\n')
	    line_no++;
	return c;	
    }
    void unget(char c) {
	if (c =='\n')
	    line_no--;
	pushback.push(c);
    }
    ~text_stream (){
	if (!eof()) 
	    warning("closing unended file");
    
	fclose(f);
    }
    String get_name() {    return name; }    

    // GNU format message.
    void message(String s)  {
	*mlog << "\n"<<get_name() << ": " << line()<<": "<<s<<endl;
    }
};


// data file: do "#" comments, read quote enclosed 
// fields 
class data_file : private text_stream
{
    
 public:
    bool rawmode;
    int line() 
{
    return text_stream::line();
}
    char data_get();
    
    void data_unget(char c) {
	unget(c);
    }

    // read line, eat \n
    String get_line();
    text_stream::get_name;
    
    // read a word till next space, leave space
    // also does quotes
    String get_word();

    // gobble horiz. white
    void gobble_white();

    // gobble empty stuff before first field.
    void gobble_leading_white();
    data_file(String s) : text_stream(s) {
	*mlog << "(" << s << flush;	
	rawmode=  false;	
    }

    ~data_file()  {
	*mlog << ")"<<flush;	
    }    

// why the $*#&^*$% redefine message/error?
    void warning(String s) {
	message("warning: " + s);
    }
    void error(String s){
	message(s);
	exit(1);    
    }
    
    text_stream::eof;
    
};


// a "const" svec. Contents can't be changed.
class Text_record : svec<String>  
{
    int line_no;
    String filename;
    
public:
    Text_record() { } // needed because of other ctor
    void message(String s) {
	*mlog << '\n'<< filename << ": "<< line_no << s << "\n";
    }	       
    String operator[](int j) {
	return svec<String>::operator[](j);
    }

    Text_record(svec<String> s, String fn, int j) : svec<String>(s) { 
	filename = fn; line_no = j; 
    }
    svec<String>::sz;           
};

class filed_stringlist : private data_file
{
 public:
    Text_record
    get_record();

    filed_stringlist(String fn):data_file(fn) {
	
    }
    data_file::error;
    data_file::eof;    
    
    //**** C++ optors
    Text_record operator++( POSTFIX_INT ) {
	return get_record();
    }
    operator bool() {
	return !eof();
    }
};


