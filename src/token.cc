/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : token.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include "config.hh"
#ifdef __unix__
#include <strstream.h>
#else
#include <strstrea.h>
#endif
#include <ctype.h>
#include "token.hh"
#include "strlist.hh"
#include "init.hh"
#include "staff.hh"
#include "mpp.hh"

istream &
char2istream( String s)
{
    static char* buf = 0;
    static istrstream* is = 0;

    if ( buf )
        delete buf;
    if ( is )
        delete is;

    buf = s.copy_array( );
    is = new istrstream( buf, s.len() );

    return *is;
}

int
Token::compare( const Object& test1, void* test2 )
{
   assert(test1 != NOOBJECT);
   assert(test2 != ZERO);
   return ((const Token&)test1 ).name ==  * (const String*) test2 ;
}

/****************************************************************************
  class Token
--*/
Token::Token( String s, const int p ) :
    parameterCount( p )
{
    name = s;
}


Token::~Token()
{
}

String
Token::_getFrom( istream& is )
{
    
    String s;
    char c;
    while (! is.eof() &&  validCharacter( c =is.peek()) )
        {
        is.get( c );
        s += c;
        c = is.peek();
        }

    return s;
}

int
Token::getFrom( Staff& staff )
{
    String s = Token::_getFrom( *staff.is );
    warning("Nonfatal internal error: Discarding token: " + s);
    
    return 0;
}

int
Token::validCharacter( const char c ) const
{
    return ( c != char( EOF ) );
}
//-- class Token //

/****************************************************************************
  class WhiteSpace
--*/

WhiteSpace::WhiteSpace( Staff& staff ) :
    length( 0 ),
    Token( "" )
{
    getFrom( staff );
}

WhiteSpace::~WhiteSpace()
{
}

int
WhiteSpace::getFrom( Staff& staff )
{
    istream& is = *staff.is;

    char c = is.peek();
    while (isspace(c) || c == '%')
        {
        is.get( c );
        length++;
        if ( c == '\n' )
	  staff.line++;       // WHUGH!
	else if (c == '%') { // eat TeX comment
	    while ((c = is.get()) != '\n')
	      length++;

	  staff.line++;
	  }
	  c = is.peek();

        }

    if ( c == char( EOF ) )
        length = min( length, -1 );

    return 0;
}

int
WhiteSpace::validCharacter( const char c ) const
{
    return ( Token::validCharacter( c ) && ( isspace( c ) || ( c == '\n' ) ) );
}

//-- class WhiteSpace //
