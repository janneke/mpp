// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : token.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __TOKEN_H
#define __TOKEN_H

#include "globals.hh"

istream& char2istream( String s );

#include "strlist.hh"

/****************************************************************************
  class Token
--*/

class Token : public Object 
{
public:
    enum Orientation 
    {
        DOWN = -1, UNDEFINED, UP
    };    

protected:
    int parameterCount;

    String _getFrom( istream& is );
    virtual int getFrom( Staff& staff );
    virtual int validCharacter( const char c ) const;

public:
    String name;

    virtual void calculate()   { }
    static int compare( const Object& test1,  void* test2 ); // test2 is a String *. Should use const?
    virtual void execute( StringList&, Staff& )
                               { }
    virtual void printOn( ostream& os ) const = 0;

    Token( String s = "" , const int p = 0 );
    virtual ~Token();
};

#define ZEROTOKEN ((Token*)ZERO)
#define NOTOKEN (*ZEROTOKEN)

//-- class Token //

#if 0
#define __TOKENCOMPARE
inline int Token::compare( const Object& test1, void* test2 )
{
    return ( (const Token&)test1 ).name.compare( (const char*)test2 );
}
#endif

#if 0
/****************************************************************************
  class MusicToken
--*/

class MusicToken : public Token {

public:
    virtual void calculate();

    MusicToken( String s = "" , const int p = 0 );
    virtual ~MusicToken();
};

#define ZEROMUSICTOKEN ((MusicToken*)ZERO)
#define NOMUSICTOKEN (*ZEROMUSICTOKEN)

//-- class MusicToken //
#endif


/****************************************************************************
  class WhiteSpace
--*/

class WhiteSpace : public Token {

protected:
    virtual int getFrom( Staff& staff );
    virtual int validCharacter( const char c ) const;
    
public:
    int length;

    virtual void printOn( ostream& os ) const
                               { os << String( ' ', length ); }
  
    WhiteSpace( Staff& staff );
    ~WhiteSpace();
};
//-- class WhiteSpace //

#endif // __TOKEN_H //
