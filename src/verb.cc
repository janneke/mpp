/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : verb.cc
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

#include <ctype.h>
#include "globals.hh"
#include "init.hh"
#include "staff.hh"
#include "verb.hh"
#include "mpp.hh"

/****************************************************************************
  class PartVerb
--*/

PartVerb::PartVerb( String s, int p ) :
    Macro( s, "", p )
{
    ;// monitor << "PartVerb::PartVerb" << endl;
}

PartVerb::~PartVerb()
{
    ;// monitor << "~PartVerb" << endl;
}

void
PartVerb::execute( StringList& parameters, Staff& )
{
    ;// monitor << "PartVerb::execute" << endl;
    substitute = "";
    if ( Staff::instrumentCount <= 2 )
        substitute += parameters.top();
}

//-- class PartVerb //

/****************************************************************************
  class ScoreVerb
--*/

ScoreVerb::ScoreVerb( String s, int p ) :
    Macro( s, "", p )
{
    ;// monitor << "ScoreVerb::ScoreVerb" << endl;
}

ScoreVerb::~ScoreVerb()
{
    ;// monitor << "~ScoreVerb" << endl;
}

void
ScoreVerb::execute( StringList& parameters, Staff& )
{
    ;// monitor << "ScoreVerb::execute" << endl;
    substitute = "";
    if ( Staff::instrumentCount > 2 )
        substitute += parameters.top();
}

//-- class ScoreVerb //

/****************************************************************************
  class Verb
--*/

Verb::Verb( String s, int p ) :
    Macro( s, "", p )
{
    ;// monitor << "Verb::Verb" << endl;
}

Verb::~Verb()
{
    ;// monitor << "~Verb" << endl;
}

void
Verb::execute( StringList& parameters, Staff& )
{
    ;// monitor << "Verb::execute" << endl;
    substitute = "";
    substitute += parameters.top();
}

//-- class Verb //
