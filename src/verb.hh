// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : verb.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/

// verb = verbatim

#ifndef __VERB_H
#define __VERB_H
#include "macro.hh"

/****************************************************************************
  class PartVerb
--*/

class PartVerb : public Macro  
{
public:
    virtual void execute( StringList& parameters, Staff& staff );

    PartVerb( String s, int p );
    virtual ~PartVerb();
};
//-- class PartVerb //

/****************************************************************************
  class ScoreVerb
--*/

class ScoreVerb : public Macro  
{
public:
    virtual void execute( StringList& parameters, Staff& staff );

    ScoreVerb( String s, int p );
    virtual ~ScoreVerb();
};
//-- class ScoreVerb //

/****************************************************************************
  class Verb
--*/

class Verb : public Macro  
{
public:
    virtual void execute( StringList& parameters, Staff& staff );
//    void printOn( ostream& os ) const;

    Verb( String s, int p );
    ~Verb();
};
//-- class Verb //

#endif // __VERB_H //
