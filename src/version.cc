/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : version.cc
  AUTHOR : J. C. Nieuwenhuizen
           jan@digicash.com
  copyright (c) FlowerSoft 1995, 1997
--*/

#include "version.hh"

const char* const VERSION_STRING = VERSION " of " __DATE__ " "
  __TIME__ " for\n% "
#include "systemna.hh"
;
