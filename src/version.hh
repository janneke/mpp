// -*-C++-*-
/****************************************************************************
  PROJECT: MusixTeX PreProcessor
  FILE   : version.h
  AUTHOR : J. C. Nieuwenhuizen

  copyright (c) FlowerSoft 1995, 1997
--*/
#ifndef __VERSION_H
#define __VERSION_H

extern const char* const VERSION_STRING;

#endif // __VERSION_H //
