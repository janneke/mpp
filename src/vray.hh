/*
  (c) Han-Wen Nienhuys 1995, 1996, 1997

  Distributed under GNU GPL  
*/

#ifndef SVEC_H
#define SVEC_H

///scaleable array, for T with def ctor.
template<class T>
class svec {
 protected:
    
    int max;

    /// the data itself
    T *thearray;

    /// stretch array.
    void remax(int newmax) {	 
	T* newarr = new T[newmax];
	size = (newmax < size) ? newmax : size;	
	for (int j=0; j<size;j++)
	    newarr[j]= thearray[j];
	
	delete[] thearray;
	thearray = newarr;
	max = newmax;
    }
    int size;

 public:

    /// report the size. See {setsize}
    int sz() const  { return size; }

    /// POST: sz() == 0
    void clear() { size = 0; }

    svec() { thearray = 0; max =0; size =0; }

    /** POST: sz() == s.
       Warning: contents are unspecified */
    void set_size(int s) {
	if (s >= max) remax(s);
	size = s;    
    }
    
    ~svec() { delete[] thearray; }

    /// return a  "new"ed copy of array 
    T* copy_array() const {
	T* Tarray = new T[size];
	for (int i=0; i<size;i++)
	    Tarray[i] = thearray[i];
	return Tarray;
    }
    operator T* () const {
	return copy_array();	
    }
    void operator=(svec const & src) {
	thearray = src.copy_array();
	max = size = src.size;	
    }
    svec(svec & src) {
	thearray = src.copy_array();
	max = size = src.size;	
    }
    void precompute () { remax(size); }

    /// this makes svec behave like an array
    T &operator[] (const int i) const {
	assert(i >=0&&i<size);
	return ((T*)thearray)[i];	
    }

    /// add to the end of array
    void add(T x) {
	if (size == max)
	    remax(2*max + 1);

	// T::operator=(T &) is called here. Safe to use with automatic
	// vars
	thearray[size++] = x;
    }

    /// junk last entry.
    void pop() { size -- ; }

    /// return last entry
    T& last(int j=0) { return thearray[size-j-1]; }
    
    bool empty() { return !size; }
};
/**

  This template implements a scaleable vector. With (or without) range
  checking. It may be flaky for objects with complicated con- and
  destructors. The type T should have a default constructor. It is
  best suited for simple types, such as int, double or String

  */

/// A simple stack based on svec.
template<class T>
class sstack : svec<T> {
 public:
    T top() { return last(); }
    T pop() {
	assert(!empty());
	T l = last();
        svec<T>::pop();
	return l;
    }
    void push(T l) { add(l); }
    bool empty() { return svec<T>::empty(); } 
};
/**
Same as for #svec# goes here.
*/
#endif
